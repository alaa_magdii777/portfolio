function generatePostHTML(id, content) {
    var html = '<div class="col-12 col-lg-6 mt-2" id="' + id + '">' +
        '<div class="card shadow">' +
        '<div class="card-body">' +
        '<p class="text-center m-0">' + content + '</p>' +
        '</div></div></div>';
    return html;
}




var counter = 1;
var postsContainer = document.getElementById('postsContainer');
var btn = document.getElementById('btn');
var post = document.getElementById('post');
btn.addEventListener('click', function () {
    // 1. Get the value of the post.
    var postValue = post.value.trim();

    // 2. Check if the length of the post is zero.
    // a) If zero => X (Alert a warning to the user.)
    // b) Else => ...
    if (postValue.length <= 0) {
        alert("You must enter content to continue!");
        return;
    }

    // 3. Generate a new HTML content.
    var newPostHTML = generatePostHTML(counter, postValue);
    counter++;

    postsContainer.innerHTML += newPostHTML;

    post.value = "";
});
