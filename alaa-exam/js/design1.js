
/*-----------------first-example------------------------

let num1;
let num2;
let result=0;
operator="";
num1=prompt("enter first num");
operator=prompt("enter operation num");
num2=prompt("enter second num");

if(operator=="+"){
	result=(num1+num2)
	console.log(result);
}else if(operator=="-"){
	result=(num1-num2);
	console.log(result);
}
else if(operator=="*"){
	result=(num1*num2);
	console.log(result);
}
else if(operator=="/"){
	result=(num1/num2);
	console.log(result);
}else {console.log("error")} 



*/




/*-----------------------for----------------------------------


let num1;
let num2;

let counter;



for(counter=1;counter<=20;counter++){
num1=prompt("enter num1");
//operator=prompt("enter operator");
num2=prompt("enter num2");
var result=num1+num2;


	console.log(counter);
	
	console.log("result"+result); 
	

}

*/


/*----------------------------------while----------------------------

var n = 0;
var x = 0;

while (n < 3) {
  n++;
  x += n;
  
  console.log(x);
}


*/



/*------------------------------push----------------------------

let arr = [1,3,5,7,9,11];
console.log(arr);


arr.push(6);

console.log(arr);


*/


/*--------------------------unshift----------------------------

let arr = [1,3,5,7,9,11];
console.log(arr);


arr.unshift(11);

console.log(arr);


*/


/*------------------------function--------------------


 function sum(x,y){
	 
	 console.log(x+y);
 }

 sum(5,5);
 
 
 */
 
 
 
/*--------------------Ano----function------------------
   
   var anon = function() {
  alert('I am anonymous');
}
anon();


*/



/*---------------let & const------------------

 {
  let x = 2;
}
// x can NOT be used here 


 var x = 10;
// Here x is 10
{
  let x = 2;
  // Here x is 2
}
// Here x is 10 

console.log(x);

//const it means can't change his value 


const y = 15 ;

console.log(y);

*/


/*----------------------object------------------


var person = {
  firstName: "John",
  lastName: "Doe",
  age: 50,
  eyeColor: "blue"
};

console.log(person.age);


*/


/*------------------ array-------------


var cars = [
  "Saab",
  "Volvo",
  "BMW"
]; 

console.log(cars[2]);


*/


/*--------------pop-------------------


var fruits = ["Banana", "Orange", "Apple", "Mango"];
document.getElementById("demo").innerHTML = fruits;

function myFunction() {
  fruits.pop();
  document.getElementById("demo").innerHTML = fruits;
}

*/



/*---------------date-------------

var date=new Date();  

console.log(date);

*/


/*----------splice------


let arr = [1,3,5,7,9,11];
console.log(arr);


arr.splice(5,1,100,200);

console.log(arr);


*/

/*---------------querySelectorAll-------

function myFunction() {
  document.querySelector(".example").style.backgroundColor = "red";
}


*/


/*-------------location-------------------


location.assign("https://www.google.com"); 


*/


/*---------map---------------


const array = [2, 5, 9];
    let squares = array.map((num) => num * num);

    console.log(squares); // [4, 25, 81]
    console.log(array); // [2, 5, 9]


	*/
	
	
	
	/*-------------filter-------------
	
	const numbers = [1, 3, 4, 6, 8, 9];

const filterValues = () => {
  return numbers.filter(number => {
      return number % 2 == 0;
  });
}

console.log(filterValues());

*/


/*------------reduce-----------

const data = [5, 10, 15, 20, 25];

const res = data.reduce((total,currentValue) => {
  return total + currentValue;
});

console.log(res); 

*/


/*----------differ --- find & findindex----*/

/*

var m=[10,20,15,30,30,30,13,-12,20,10];

var m2= m.find(function(el){

   return el %2 !==0 ;
  });
  
  console.log(m2); 
  */

/*
let myArr = [9, 3, 9, 4, 9, 11, 19];

let ind = myArr.findIndex((v)=>{
    return  v % 2 === 0;
});    

if(ind !== -1) {
      console.log("Findex", myArr[ind])
}
*/

/*-------innerText------------& innerHtml---------

function myFunction() {
  var x = document.getElementById("myBtn").innerText;
  document.getElementById("demo").innerHTML = x;  
}

*/


/*------------different--tags-------------*/


/*
<a> anchor to make location of url 

<a href="#"> Go to</a>

*/

/*
<b> to make text bold

<b><h1>Alaa</h1> </b>
*/

/*
<p> to make a paragrph

  <p>my name is alaa ......, live in .... ,</p>
  
  */
  
/*
<u> to make underline of text

<u><h6>underline</h6></u>
*/

/*
<i> to make the text italic 

<i><h6>italic</h6></i>
*/

/*
<s> it means cros in text 
<p><s>My car is blue.</s></p>

*/

/*
<q> the <q> tag defines a short quotation

<p>WWF's goal is to: 
<q>Build a future where people live in harmony with nature.</q>
We hope they succeed.</p>


*/