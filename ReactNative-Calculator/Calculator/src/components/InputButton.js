
import React, { Component } from 'react';
import {
  StyleSheet,Text,TouchableHighlight} from 'react-native';

export default class InputButton extends Component{


    constructor(props) {
        super(props);
    
      }
    
  render(){
  return (

     
     <TouchableHighlight style={[styles.inputButton , this.props.highlight  ? styles.inputButtonHighlight:null] }
       
            onPress={this.props.onPress}  underlayColor='#feb72b'>

            <Text style={styles.inputButtonText}>{this.props.value}</Text>

     </TouchableHighlight>



  );}
};

const styles = StyleSheet.create({
  
  inputButton:{
      flex:1,
      alignItems:'center',
      borderWidth:5,
      justifyContent:"center",
      borderColor:"black",

  },
  inputButtonText:{
      fontSize:25,
      fontWeight:'bold',
      color:"black",

  },

  inputButtonHighlight:{
    
 },

});


