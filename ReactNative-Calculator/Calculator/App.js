import React, { Component } from 'react';
import {StyleSheet, ScrollView,View,Text,TouchableHighlight} from 'react-native';
import InputButton from "./src/components/InputButton"



const InputButtons=[

  [1,2,3,'+'],
  [4,5,6,'-'],
  [7,8,9,'/'],
  [0,'C','*','='],
  

];

export default class App extends Component{



  constructor(props) {
    super(props);
    this.intialState = {
      previousInputValue: 0,
      selectedSymbol: null,
      inputValue: 0
    }
    this.state = this.intialState;
  }

  
  render(){
  
    return (


     <View style={styles.rowcontainer}>
          
          <View style={styles.displaycontainer}>
                <Text style={{fontSize:40}}>{this.state.inputValue}</Text>
          </View>

          <View style={styles.inputcontainer}>{this._renderInputButtons()}</View>
    </View>
  
  );

}



_renderInputButtons() {

    let views = [];
    /*loop-for-rows*/
    for (let r = 0; r < InputButtons.length; r++) {
    let row = InputButtons[r];

    /*loop-for-elements-of-rows*/
    let inputRow = [];
    for (let i = 0; i < row.length; i++) {
      let input = row[i];
      inputRow.push(<InputButton
        onPress={this._onInputButtonPressed.bind(this, input)}
        value={input} key={r + '-' + i} />);
    }
    views.push(<View style={styles.inputRow} key={"row" + r}>{inputRow}</View>)
  }
  return views;

}





 _onInputButtonPressed(input) {
    switch (typeof input) {
      case 'number': return this._handleNumberInput(input);
      default: return this._handleStringInput(input);
    }
  }
  _handleNumberInput(num) {
    let currentInputValue = (this.state.inputValue * 10) + num;
    this.setState({
      inputValue: currentInputValue
    })
  }
  _handleStringInput(str) {
    switch (str) {
      case '/':
      case '*':
      case '+':
      case '-':
        this.setState({
          selectedSymbol: str,
          previousInputValue: this.state.inputValue,
          inputValue: 0
        })
      case '=':
        let previousInputValue = this.state.previousInputValue;
        let symbol = this.state.selectedSymbol;
        let currentInputValue = this.state.inputValue;
        if (!symbol) return;
        this.setState({
          selectedSymbol: null,
          previousInputValue: 0,
          inputValue: eval(previousInputValue + symbol + currentInputValue)
        });
        break;
      case 'C':
        this.setState({ inputValue: 0 })
        break;
    }

  }


};

const styles = StyleSheet.create({
  

  rowcontainer:{flex:1,},
  displaycontainer:{flex:3,backgroundColor:'#feb72b',padding:20},
  inputcontainer:{flex:7,},

  inputRow:{flex:1,flexDirection:'row',}

});


