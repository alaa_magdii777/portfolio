// Functions
// User-Defined:
// - Normal.
// - Anonymous.
// - Self-Invoked.
// - IIFE


// Built-in Functions.

// Escape Characters
//

// Template.....
function generateCardHTML(id, name) {
    let html = '<div class="col-12 col-md-6 mt-2" id="' + id + '">' +
        '<div class="card shadow">' +
        '<div class="card-body">' +
        '<p class="m-0 font-weight-bold text-center">' +
        name + '</p></div></div></div>'

    return html;
}

for (let i = 0; i < 0; i++) {
    // Find the username.
    var username = prompt("Enter your name:");

    // When the username is empty, repeat this step.
    if (username.length <= 0) {
        i--;
        continue;
    }

    // Find a new id.
    var newID = "new-user-" + (i + 1);
    // Generate the HTML string.
    var html = generateCardHTML(newID, username);
    // Add the content to the webpage.
    var cardsContainer = document.getElementById('cardsContainer');
    cardsContainer.innerHTML += html;
}

// // Arrays.
// var arr1 = [];
// var arr2 = [1, 2, 10.5, "aa", 1, true, [], []];
// // Index => 0 (Starting Index.)
// console.log(arr2[3]);
//
// // for (let i = 0; i < arr2.length; i++) {
// //     console.log(arr2[i]);
// // }
//
// arr2.push(1000);
// arr2.push(1000);
// //arr2.push(arr2);
// console.log(arr2);
// // arr2.splice(2, arr2.length);
// arr2.splice(2, 0, 10, 20, 30);
// console.log(arr2);

// Add an element at the end of the array.
// Add an element at index 2.
// Remove an element at index 7.
// Replace an element at index 4.
// Replace an element at index 5 with another two elements.
// Remove the last 3 elements of an array. // TASK
// Remove the first 3 elements of an array. // TASK

//
// let arr5 = [1, 800, 300, 400, 300, 2000];
//
// // 1)
// arr5.push(4);
// console.log(arr5);
// // 2)
// arr5.splice(2, 0, 500);
// console.log(arr5);
// // 3)
// arr5.splice(7, 1);
// console.log(arr5);
// // 4)
// arr5.splice(4, 1, "horya");
// console.log(arr5);
// // 5)
// arr5.splice(5, 1, 20, 30);
// console.log(arr5);
// // 6)
// arr5.splice(arr5.length - 3, 3);
// console.log(arr5);
// // 7)
// arr5.splice(0, 3);
// console.log(arr5);

// let arr6 = [1, 800, "300", 400, "300", 2000];
//
// var s = 0;
// for (let i = 0; i < arr6.length; i++) {
//     s += parseFloat(arr6[i]);
// }
// console.log(s);
// 8013004003002000


// Timeout
// setTimeout(function () {
//     alert("مبروك كسبت ايفون!");
// }, 3000);

// Interval
// var c = 1;
// setInterval(function () {
//     console.log("CHECKING NEW MESSAGES!" + c++);
// }, 2000);

// TASK
// 3 Minutes.


// Recursion.
// function repeatTimeout() {
//     setTimeout(function () {
//         alert("مبروك كسبت ايفون!");
//         repeatTimeout();
//     }, 3000);
// }
// repeatTimeout();

// var c = 1;
// var si1 = setInterval(function () {
//     console.log("CHECKING NEW MESSAGES!" + c++);
//     clearInterval(si1);
// }, 2000);


// Objects.
// var obj1 = {
//     firstName: "Ali",
//     lastName: "Ali2",
//     email: "ali@ali.ali",
//     username: "Ali3",
// };
//
// console.log(obj1);
// console.log(obj1['firstName']);
// console.log(obj1.firstName);
// obj1['gender'] = 'Male';
// obj1['email'] = 'ali@ali.com';
// console.log(obj1);


// var testObj = {
//     first: [1, 2, 3, {t: 10}],
//     second: {n: [10, 20, 30]},
//     third: [{d: [{l: 1000}]}]
// };
//
// console.log(testObj.first[2]);
// console.log(testObj.first[3].t);
// console.log(testObj.second.n[1]);
// console.log(testObj.third[0].d[0]);
// console.log(testObj.third[0].d[0].l);

// Value
// JSON: JavaScript Object Notation.
// [CLIENT] ===[JSON]=== [SERVER]


var quickTest = [2, {a: {b: {c: [1, {d: {e: [20, {}, 10]}}]}}}];

// {a: {b: {c: [1, {d: {e: [20, {}, 10]}}]}}}
// {b: {c: [1, {d: {e: [20, {}, 10]}}]}}
// {c: [1, {d: {e: [20, {}, 10]}}]}
// [1, {d: {e: [20, {}, 10]}}]
// {d: {e: [20, {}, 10]}}
// {e: [20, {}, 10]}
// [20, {}, 10]
// {}
console.log(quickTest[1].a.b.c[1].d.e[1]);

// Client => Server
let json = JSON.stringify(quickTest);
console.log(json);

// Server => Client
let obj = JSON.parse(json);
console.log(obj);

// AJAX.


// var jsonStr = '[\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 1,\n' +
//     '    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",\n' +
//     '    "body": "quia et suscipit\\nsuscipit recusandae consequuntur expedita et cum\\nreprehenderit molestiae ut ut quas totam\\nnostrum rerum est autem sunt rem eveniet architecto"\n' +
//     '  },\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 2,\n' +
//     '    "title": "qui est esse",\n' +
//     '    "body": "est rerum tempore vitae\\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\\nqui aperiam non debitis possimus qui neque nisi nulla"\n' +
//     '  },\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 3,\n' +
//     '    "title": "ea molestias quasi exercitationem repellat qui ipsa sit aut",\n' +
//     '    "body": "et iusto sed quo iure\\nvoluptatem occaecati omnis eligendi aut ad\\nvoluptatem doloribus vel accusantium quis pariatur\\nmolestiae porro eius odio et labore et velit aut"\n' +
//     '  },\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 4,\n' +
//     '    "title": "eum et est occaecati",\n' +
//     '    "body": "ullam et saepe reiciendis voluptatem adipisci\\nsit amet autem assumenda provident rerum culpa\\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\\nquis sunt voluptatem rerum illo velit"\n' +
//     '  },\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 5,\n' +
//     '    "title": "nesciunt quas odio",\n' +
//     '    "body": "repudiandae veniam quaerat sunt sed\\nalias aut fugiat sit autem sed est\\nvoluptatem omnis possimus esse voluptatibus quis\\nest aut tenetur dolor neque"\n' +
//     '  },\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 6,\n' +
//     '    "title": "dolorem eum magni eos aperiam quia",\n' +
//     '    "body": "ut aspernatur corporis harum nihil quis provident sequi\\nmollitia nobis aliquid molestiae\\nperspiciatis et ea nemo ab reprehenderit accusantium quas\\nvoluptate dolores velit et doloremque molestiae"\n' +
//     '  },\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 7,\n' +
//     '    "title": "magnam facilis autem",\n' +
//     '    "body": "dolore placeat quibusdam ea quo vitae\\nmagni quis enim qui quis quo nemo aut saepe\\nquidem repellat excepturi ut quia\\nsunt ut sequi eos ea sed quas"\n' +
//     '  }]';
//
// let obj2 = JSON.parse(jsonStr);
// console.log(obj2);
//
// for (let i = 0; i < obj2.length; i++) {
//     console.log(obj2[i].title);
//     console.log(obj2[i].id);
//     console.log(obj2[i].body);
//     console.log(obj2[i].userId);
// }
//
// let  json2 ='[\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 1,\n' +
//     '    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",\n' +
//     '    "body": "quia et suscipit\\nsuscipit recusandae consequuntur expedita et cum\\nreprehenderit molestiae ut ut quas totam\\nnostrum rerum est autem sunt rem eveniet architecto"\n' +
//     '  },\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 2,\n' +
//     '    "title": "qui est esse",\n' +
//     '    "body": "est rerum tempore vitae\\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\\nqui aperiam non debitis possimus qui neque nisi nulla"\n' +
//     '  },\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 3,\n' +
//     '    "title": "ea molestias quasi exercitationem repellat qui ipsa sit aut",\n' +
//     '    "body": "et iusto sed quo iure\\nvoluptatem occaecati omnis eligendi aut ad\\nvoluptatem doloribus vel accusantium quis pariatur\\nmolestiae porro eius odio et labore et velit aut"\n' +
//     '  },\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 4,\n' +
//     '    "title": "eum et est occaecati",\n' +
//     '    "body": "ullam et saepe reiciendis voluptatem adipisci\\nsit amet autem assumenda provident rerum culpa\\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\\nquis sunt voluptatem rerum illo velit"\n' +
//     '  },\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 5,\n' +
//     '    "title": "nesciunt quas odio",\n' +
//     '    "body": "repudiandae veniam quaerat sunt sed\\nalias aut fugiat sit autem sed est\\nvoluptatem omnis possimus esse voluptatibus quis\\nest aut tenetur dolor neque"\n' +
//     '  },\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 6,\n' +
//     '    "title": "dolorem eum magni eos aperiam quia",\n' +
//     '    "body": "ut aspernatur corporis harum nihil quis provident sequi\\nmollitia nobis aliquid molestiae\\nperspiciatis et ea nemo ab reprehenderit accusantium quas\\nvoluptate dolores velit et doloremque molestiae"\n' +
//     '  },\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 7,\n' +
//     '    "title": "magnam facilis autem",\n' +
//     '    "body": "dolore placeat quibusdam ea quo vitae\\nmagni quis enim qui quis quo nemo aut saepe\\nquidem repellat excepturi ut quia\\nsunt ut sequi eos ea sed quas"\n' +
//     '  },\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 8,\n' +
//     '    "title": "dolorem dolore est ipsam",\n' +
//     '    "body": "dignissimos aperiam dolorem qui eum\\nfacilis quibusdam animi sint suscipit qui sint possimus cum\\nquaerat magni maiores excepturi\\nipsam ut commodi dolor voluptatum modi aut vitae"\n' +
//     '  },\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 9,\n' +
//     '    "title": "nesciunt iure omnis dolorem tempora et accusantium",\n' +
//     '    "body": "consectetur animi nesciunt iure dolore\\nenim quia ad\\nveniam autem ut quam aut nobis\\net est aut quod aut provident voluptas autem voluptas"\n' +
//     '  },\n' +
//     '  {\n' +
//     '    "userId": 1,\n' +
//     '    "id": 10,\n' +
//     '    "title": "optio molestias id quia eum",\n' +
//     '    "body": "quo et expedita modi cum officia vel magni\\ndoloribus qui repudiandae\\nvero nisi sit\\nquos veniam quod sed accusamus veritatis error"\n' +
//     '  }]'
//
// let json8 =JSON.parse(json2);
// console.log(json8);
// for ( i=0;i<json8.length;i++)
// {
//     console.log(json8[i].id);
//     console.log(json8[i].title);
//     console.log(json8[i].body);
//     console.log(json8[i].userId);
// }




