var input = document.getElementsByTagName('input');
var form = document.getElementById('form');

for(var i = 0; i < input.length; i++) {
	input[i].addEventListener('click', function() {
			if(this.parentNode.classList.contains('active')) {
				this.parentNode.classList.remove('active');
			} else {
        this.parentNode.classList.add('active');
      }
	});
}

window.addEventListener('click', function(e) {
  if(form.contains(e.target) == false) {
    for(var j = 0; j < input.length; j++) {
      input[j].parentNode.classList.remove('active');
    }
  }
});