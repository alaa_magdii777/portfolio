<?php
    //images are in format of: 
    //#x.gif
    //Where # is card value
    //Where "x" is card suit
    
    //clubs = c
    //diamonds = d
    //hearts = h
    //spades = s
    
    //Select 7 cards with the for loop.
    for( $i = 1; $i <= 7; $i++ ) {
        //select card, define suit as numeric value
        $card["card" . $i]['suit_num'] = rand(0,3);
        //select card, define card itself as numeric value
        $card["card" . $i]['card_num'] = rand(1,13);
        //redefine card's suit as character value assigning either c, d, h, or s
        //This is used for image filepath
        switch ($card["card" . $i]['suit_num']) {
            case 0:
                $card["card" . $i]['suit_char'] = "c";
                $card["card" . $i]['suit_str'] = "Clubs";
                break;
            case 1:
                $card["card" . $i]['suit_char'] = "d";
                $card["card" . $i]['suit_str'] = "Diamonds";
                break;
            case 2:
                $card["card" . $i]['suit_char'] = "h";
                $card["card" . $i]['suit_str'] = "Hearts";
                break;
            case 3:
                $card["card" . $i]['suit_char'] = "s";
                $card["card" . $i]['suit_str'] = "Spades";
                break;
        }
        //define card filepath and store in "card_img"
        $card["card" . $i]['card_img'] = "cards/" . $card["card" . $i]['card_num'] . $card["card" . $i]['suit_char'] . ".gif";
        //Define the Card Itself in the form of words.
        switch($card["card" . $i]['card_num']) {
            case 1: 
                $card["card" . $i]['card_str'] = "Ace of ";
                break;
            case 2: 
                $card["card" . $i]['card_str'] = "Two of ";
                break;
            case 3: 
                $card["card" . $i]['card_str'] = "Three of ";
                break;
            case 4: 
                $card["card" . $i]['card_str'] = "Four of ";
                break;
            case 5: 
                $card["card" . $i]['card_str'] = "Five of ";
                break;
            case 6: 
                $card["card" . $i]['card_str'] = "Six of ";
                break;
            case 7: 
                $card["card" . $i]['card_str'] = "Seven of ";
                break;
            case 8: 
                $card["card" . $i]['card_str'] = "Eight of ";
                break;
            case 9: 
                $card["card" . $i]['card_str'] = "Nine of ";
                break;
            case 10: 
                $card["card" . $i]['card_str'] = "Ten of ";
                break;
            case 11: 
                $card["card" . $i]['card_str'] = "Jack of ";
                break;
            case 12: 
                $card["card" . $i]['card_str'] = "Queen of ";
                break;
            case 13: 
                $card["card" . $i]['card_str'] = "King of ";
                break;
        }
        //Combine card_str and suit_str into one for the table headers
        $card["card" . $i]['card_info'] = $card["card" . $i]['card_str'] . $card["card" . $i]['suit_str'];
    }
    //save old hand to file    
    //Open File
    $myFile = "old_hands.php";
    $lines = file($myFile);
    $fh = fopen($myFile, 'w+') or die("can't open file");
    //add changes to new variable
    $changes = "";
    for($i = 1; $i <= 7; $i++) {
        $changes .= "Card " . $i . ": " . $card["card" . $i]['card_info'] . "<br>\n";
    }    
    $changes = $changes . "<br>\n<br>\n";
    //write changes to new file
    fwrite($fh, $changes);
    foreach ($lines as $line) { fwrite( $fh, "$line"); }
    //define from what line ONWARD/DOWN to delete
    $lineNum = 46;
    //Delete Lines Function
    function delLineFromFile($fileName, $lineNum){
        //Delete 10 lines (One entry) 1 line at a time
        for($i = 1; $i <= 10; $i++) {
            // read the file into an array    
            $arr = file($fileName);
            // the line to delete is the line number minus 1, because arrays begin at zero
            $lineToDelete = $lineNum-1;
            //remove the line
            unset($arr["$lineToDelete"]);
            // open the file for reading
            $fp = fopen($fileName, 'w+');
            // write the array to the file
            foreach($arr as $line) { fwrite($fp,$line); }
        }
    }
    //we dont want any more then 5 entries to delet the last/6th entry
    delLineFromFile($myFile, $lineNum);
    //Only display lines you specify function
    function sumarize($your_string, $limit_lines){
       $count = 0;
       foreach(explode("\n", $your_string) as $line){
           $count++;
           echo $line."\n";
           if ($count == $limit_lines) break;        
       }
    }

?>
<html>
<head>
    <title>
        Coding Forums Post 266395: Card Generator
    </title>
    <link rel="stylesheet" type="text/css" href="index.css" />
</head>
<body>
    <input type="submit" value="Pick a New Hand!" onclick="javascript:location.reload(true)" />
    <br>
    <br>
    <table id="card_table">
        <tr>
            <?php
                //display card information in table headers
                for($i = 1; $i <= 7; $i++ ) {
                    echo("<th>" . $card["card" . $i]['card_info'] . "</th>");
                }
            ?>
        </tr>
        <tr>
            <?php
                //display cards
                for($i = 1; $i <= 7; $i++ ) {
                    echo("<td class=\"card_img\"><img src=\"img/1.jpg" . $card["card" . $i]['card_img'] . "\" /></td>");
                }
            ?>
        </tr>
        <tr>
            <td colspan="7" id="old_hands">
                <span class="header">All hands displayed below are in order from most recent to less recent.</span>
                <br>
                <br>
                <?php
                    //Only display lines 1-43 as we dont want to display the last two lines as they are <br> tags
                    echo(sumarize(file_get_contents($myFile), 43)); 
                ?>
            </td>
        </tr>
    </table>
</body>
</html>
<?php
fclose($fh);
?>