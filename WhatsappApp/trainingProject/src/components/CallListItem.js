import React from 'react';
import { ListItem,Icon} from 'react-native-elements';
import {View, Text,TouchableHighlight,StyleSheet,Image,ScrollView,TouchableOpacity} from 'react-native';
import colors from "../utilis/colors";
import PropTypes from 'prop-types';
// import Icon from 'react-native-vector-icons';


export default function CallListItem ({name,avatar,message,iconsF,iconsS,arrow,onPress}){
   
    const renderLeftElement=()=>   
        <View style={styles.containerInfo}>
            <View style={styles.contactInfo}>
                <Image style={styles.avatar} source={(avatar)} />
            </View>

            <View style={styles.nameMessageInfo}>
                <Text style={styles.title}>{name}</Text>
                <View style={{flexDirection:'row',}}>
                    <Image style={{height:18,width:15,marginRight:10}} source={(arrow)} />  
                    <Text style={styles.subtitle}>{message}</Text> 
                </View> 
            </View>
        </View>;
        
    const renderRightElement=()=>   
    <View style={styles.contactInfo}>
         <View style={{flexDirection:'row'}}>
             <TouchableOpacity style={{flexDirection:"column",padding:3,paddingRight:12}}>
                  <Icon
                      type='font-awesome'
                      name={iconsF}
                      color="#075E54"
                      size={26}
                   />
             </TouchableOpacity>

             <TouchableOpacity style={{flexDirection:"column",padding:3,paddingLeft:12}}>
                    <Icon
                      type='material'
                      name={iconsS}
                      color="#075E54"
                      size={26}
                    />
             </TouchableOpacity>
         </View>     
    </View>;

    return (
        <ScrollView style={styles.scrollView}>
            <ListItem onPress={onPress}
                Component={TouchableOpacity}
                containerStyle={styles.container}
                rightElement={renderRightElement() } 
                leftElement={renderLeftElement()}
                bottomDivider={true}       
            />
        </ScrollView>
    )}

CallListItem.PropTypes={
    name:PropTypes.string,
    avatar:PropTypes.string.isRequired,
    message:PropTypes.string.isRequired,
    iconsF:PropTypes.string,
    iconsS:PropTypes.string,
    arrow:PropTypes.string
}    
const styles=StyleSheet.create({
    container:{
        flex:1,
        padding:-5,
        textAlign:'center',
        paddingBottom:10,
        paddingTop:10,
        flexDirection:'row',
    },
    containerInfo:{
        flexDirection:'row',alignItems:"center",
    },
    contactInfo:{
        paddingTop:0,
        paddingBottom:16,
        flexDirection:'column',alignItems:"center",
    },
    nameMessageInfo:{
        flexDirection:'column', padding:10,paddingBottom:20
    },
    avatar:{
        height:60,
        width:60,
        borderRadius:50
    },
    details:{
        justifyContent:'center',
        flexDirection:'column',alignItems:"center"
    },
    title:{
        color:colors.black,
        fontSize:16,padding:4,fontWeight:"bold"
    },
    subtitle:{
        color:colors.grey,
        fontSize:16,
    },
    scrollView: {
        marginHorizontal: 20,
      },
})


