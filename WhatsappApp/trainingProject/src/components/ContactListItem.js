import React from 'react';
import { ListItem} from 'react-native-elements';
import {View, Text,TouchableHighlight,StyleSheet,Image,ScrollView,TouchableOpacity} from 'react-native';
import colors from "../utilis/colors";
import PropTypes from 'prop-types';
export default function ContactListItem ({name,avatar,message,time,onPress}){
   
    const renderLeftElement=()=>   
        <View style={styles.containerInfo}>
            <View style={styles.contactInfo}>
                <Image style={styles.avatar} source={(avatar)} />
            </View>

            <View style={styles.nameMessageInfo}>
                <Text style={styles.title}>{name}</Text>
                <Text style={styles.subtitle}>{message}</Text>    
            </View>
        </View>;
        
    const renderRightElement=()=>   
    <View style={styles.contactInfo}>
        <Text style={styles.title}>{time}</Text>
    </View>;

    return (
        <ScrollView style={styles.scrollView}>
            <ListItem onPress={onPress}
                Component={TouchableOpacity}
                containerStyle={styles.container}
                rightElement={renderRightElement() } 
                leftElement={renderLeftElement()}
                bottomDivider={true}       
            />
        </ScrollView>
    )}

ContactListItem.PropTypes={
    name:PropTypes.string,
    avatar:PropTypes.string.isRequired,
    message:PropTypes.string.isRequired,
    time:PropTypes.float
}    
const styles=StyleSheet.create({
    container:{
        flex:1,
        padding:-5,
        textAlign:'center',
        paddingBottom:10,
        paddingTop:10,
        flexDirection:'row',
    },
    containerInfo:{
        flexDirection:'row',alignItems:"center",
    },
    contactInfo:{
        paddingTop:0,
        paddingBottom:16,
        flexDirection:'column',alignItems:"center",
    },
    nameMessageInfo:{
        flexDirection:'column', padding:10,paddingBottom:20
    },
    avatar:{
        height:60,
        width:60,
        borderRadius:50
    },
    details:{
        justifyContent:'center',
        flexDirection:'column',alignItems:"center"
    },
    title:{
        color:colors.black,
        fontSize:16,fontWeight:"bold",
    },
    subtitle:{
        color:colors.black,padding:4,
        fontSize:16
    },
    scrollView: {
        marginHorizontal: 20,
      },
})


