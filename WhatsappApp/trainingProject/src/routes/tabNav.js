import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { createAppContainer } from 'react-navigation';
// import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from "react-navigation-stack";
import Chats from "../screens/Contacts";
import Call from "../screens/Call";
import Status from "../screens/Status";
// import index from "./index"
import {Icon} from 'react-native-elements';
import colors from "../utilis/colors"
import Profile from "../screens/Profile";
import Chat from "../screens/Chat"
import ContactsApi from "../screens/ContactsApi"

// const container=()=>{
//   <View style={{ flexDirection: "row",}}>
//   <Text>aaaaaaaaaaaa</Text>
// </View>
// }

const ContactsScreen=createStackNavigator({
  // Chats:{
  //   screen: Chats,
  //   navigationOptions:({navigation})=>({
  //     headerShown:false,
  // })},

  ContactsApi:{
    screen:ContactsApi,
    navigationOptions:({navigation})=>({
          headerShown:false,
  })},
    Profile:{
      screen: Profile,
      navigationOptions:({navigation}) => ({      
      headerBackTitleVisible:true,     
    })},

    Chat:{
      screen: Chat,
      navigationOptions:({navigation}) => ({      
      headerBackTitleVisible:false,     
    })},
    
    
});
    

const CallScreen=createStackNavigator({
  Call:{
    screen: Call,
    navigationOptions:({navigation})=>({headerShown:false}
    )}
});




const StatusScreen=createStackNavigator({
  Status:{
    screen: Status,
    navigationOptions:({navigation})=>({headerShown:false, 
  }
    )}
});



      
  

const TabNavigator = createMaterialTopTabNavigator({ 
  // Chats:ContactsScreen,
  ContactsApi:ContactsScreen,
  Status:StatusScreen,
  Call:CallScreen,
},

{
  // initialRouteName: 'Chats',
  initialRouteName: 'ContactsApi',
  // tabBarPositions: 'bottom',
   tabBarOptions: {
      style: { backgroundColor: colors.greendark,},
      
    
//       showLabel: false,
//       showIcon: true,
//       // activeTintColor: colors.white,
//       inactiveTintColor: colors.greyDark
  
}});

// export default createAppContainer(TabNavigator);




const AppContainer=createAppContainer(TabNavigator);

export default class tabNav extends React.Component {

  constructor(props) {
    super(props);
}

  render(){
    let {navigation}=this.props;
    console.log("tab",this.props.navigation)
      return(
        <AppContainer/>  
        )
  }
}
