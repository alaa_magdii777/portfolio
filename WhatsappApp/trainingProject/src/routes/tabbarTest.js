import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
// import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import Chat from "../screens/Chat";
import Call from "../screens/Call";
import Status from "../screens/Status";
import index from "./index"
import {Icon} from 'react-native-elements';
import colors from "../utilis/colors"

class ChatScreen extends React.Component {
    render() {
      return (
       <Chat/>
      );
    }
  }



  class StatusScreen extends React.Component {
    render() {
      return (
        <Status/>
      );
    }
  }



  class CallScreen extends React.Component {
    render() {
      return (
        <Call/>
      );
    }
  }




  

//const TabNavigator=createMaterialTopTabNavigator({


//      Chat:{screen:ChatScreen,

//         navigationOptions:{
//             tabBarLabel:'Chat',
//             tabBarIcon:()=>(
//             <Icon type="font-awesome" name="home" color={"#7E7E7E"} size={30} />

//             ),            
//              tabBarOptions: { activeTintColor:'white'}
//         }
// },
          

    
//      Call:{screen:CallScreen,
    
//       navigationOptions:{
//         tabBarLabel:'Status',
//         tabBarIcon:()=>(
//         <Icon type="font-awesome" name="home" color={"#7E7E7E"} size={30} />

//         ),            
//          tabBarOptions: { activeTintColor:'white'}
//     }

//     },
     

//      Status:{screen:StatusScreen,
//       navigationOptions:{
//         tabBarLabel:'Call',
//         tabBarIcon:()=>(
//         <Icon type="font-awesome" name="home" color={"#7E7E7E"} size={30} />

//         ),            
//          tabBarOptions: { activeTintColor:'white'},
         
//     }
//     },

//})



//------------------------------------
const TabNavigator = createBottomTabNavigator({ 

  
  Chat:Chat,
  Call:Call,
  Status:Status
},


{
  initialRouteName: 'Chat',
  tabBarPositions: 'bottom',
  tabBarOptions: {
      style: { backgroundColor: colors.greyLigth },
      showLabel: false,
      showIcon: true,
      activeTintColor: colors.blue,
      inactiveTintColor: colors.greyDark
  }
}
);


//------------------------------------

    
  







export default createAppContainer(TabNavigator);
