import React, {Component } from 'react'
import {View,FlatList,Image,Text} from 'react-native'
import StatusListItem from "../components/StatusListItem";
import Profile from "../screens/Profile";
const keyExtractor=({id})=>id;
export default class Status extends Component {
    constructor(props) {
        super(props);
        console.log(this.props,"eeeeeeh");
      }

    contacts=[{id:'1',name:'Alaa Magdy',avatar:require('../assets/images/avatar1.jpg'),time:'yesterday'},
              {id:'2',name:'Yumna Magdy',avatar:require('../assets/images/avatar4.jpg'),time:'8:28 pm'},
              {id:'3',name:'Magdy Ibrahim',avatar:require('../assets/images/avatar3.jpg'),time:'12 am'},
              {id:'4',name:'Asmaa Eid',avatar:require('../assets/images/avatar2.jpg'),time:'1 pm'},
              {id:'5',name:'Yumna Magdy',avatar:require('../assets/images/avatar4.jpg'),time:'8:28 pm'},
              {id:'6',name:'Magdy Ibrahim',avatar:require('../assets/images/avatar3.jpg'),time:'12 am'},
              {id:'7',name:'Abdo Magdy',avatar:require('../assets/images/avatar5.jpg'),time:'5 am'},
              {id:'8',name:'Menna Magdy',avatar:require('../assets/images/avatar6.jpg'),time:'yesterday'}
            ]
    
              renderContact = ({ item }) => {
                const { id, name, avatar,time,} = item;
                let {navigation}=this.props;

                return (
                    <StatusListItem 
                        name={name}
                        avatar={avatar}
                        time={time}
                        // onPress={() =>navigation.navigate('Profile', { id })}
                        onPress={()=>{navigation.navigate('Profile', { id })}}
                    />
                    ); };        
    
     render() {
        return (
            
            <View>

               <View style={{flexDirection:"row",paddingLeft:10,backgroundColor:"white",padding:5,}}>
                    <View style={{flexDirection:"column",padding:10,}}>
                    <Image style={{height:60,width:60}} source={require('../assets/images/addStory.png')} />
                    </View>

                    <View style={{flexDirection:"column",paddingLeft:10,padding:10,paddingTop:15}}>
                        <Text>My Status</Text>
                        <Text>tap to add status update</Text>
                    </View>
               </View>

               <Text style={{color:"#075E54",fontSize:19,paddingLeft:20,padding:10,}}>Recent updates</Text>

               <FlatList style={{backgroundColor:'white'}}
                data={this.contacts}
                renderItem={this.renderContact}
                keyExtractor={keyExtractor}/>
            </View>
       )
    }
}
