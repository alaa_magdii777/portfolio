import React, {Component } from 'react'
import {View,FlatList } from 'react-native'
import CallListItem from "../components/CallListItem"

const keyExtractor=({id})=>id;
export default class Call extends Component {
    constructor(props) {
        super(props);
        console.log(this.props,"Call");
      }
 
    contacts=[{id:'1',name:'Alaa Magdy',avatar:require('../assets/images/avatar1.jpg'),message:"Today 10:14 AM",iconsF:'phone',iconsS:'videocam',arrow:require('../assets/images/arrowup.png')},
              {id:'2',name:'Yumna Magdy',avatar:require('../assets/images/avatar4.jpg'),message:"Today 1:10 PM",iconsF:'phone',iconsS:'videocam',arrow:require('../assets/images/arrowup.png')},
              {id:'3',name:'Magdy Ibrahim',avatar:require('../assets/images/avatar3.jpg'),message:"Today 12:14 AM",iconsF:'phone',iconsS:'videocam',arrow:require('../assets/images/arrowup.png')},
              {id:'4',name:'Asmaa Eid',avatar:require('../assets/images/avatar2.jpg'),message:"Yesterday 10:14 AM",iconsF:'phone',iconsS:'videocam',arrow:require('../assets/images/arrowup.png')},
              {id:'5',name:'Yumna Magdy',avatar:require('../assets/images/avatar4.jpg'),message:"Now 9:00 AM",iconsF:'phone',iconsS:'videocam',arrow:require('../assets/images/arrowup.png')},
              {id:'6',name:'Magdy Ibrahim',avatar:require('../assets/images/avatar3.jpg'),message:"Yesterday 10:14 AM",iconsF:'phone',iconsS:'videocam',arrow:require('../assets/images/arrowup.png')},
              {id:'7',name:'Abdo Magdy',avatar:require('../assets/images/avatar5.jpg'),message:"Today 7:5 AM",iconsF:'phone',iconsS:'videocam',arrow:require('../assets/images/arrowup.png')},
              {id:'8',name:'Menna Magdy',avatar:require('../assets/images/avatar6.jpg'),message:"Yesterday 7:14 AM",iconsF:'phone',iconsS:'videocam',arrow:require('../assets/images/arrowup.png')}
            ]
    
              renderContact = ({ item }) => {
                const { id, name, avatar, message,iconsF,iconsS,arrow} = item;
                let {navigation}=this.props;

                return (
                    <CallListItem
                        name={name}
                        avatar={avatar}
                        message={message}
                        iconsF={iconsF}
                        iconsS={iconsS}
                        arrow={arrow}
                        onPress={()=>{navigation.navigate('Profile', { id })}}
                    />
                    ); };        
    
     render() {
        return (
            <View>
               <FlatList style={{backgroundColor:'white'}}
                data={this.contacts}
                renderItem={this.renderContact}
                keyExtractor={keyExtractor}/>
            </View>
       )
    }
}
