import React, { Component } from 'react'
import { Text, View } from 'react-native'
import Contacts from "./Contacts"
import {GiftedChat,Bubble,InputToolbar,} from 'react-native-gifted-chat'


export default class Chat extends Component {

     //Gifted
     state = {
        messages: [],
      };
     
      componentDidMount() {
        this.setState({
          messages: [
            {
              _id: 1,
              text: 'Hello developer',
              createdAt: new Date(),
              user: {
                _id: 2,
                name: 'React Native',
              //  avatar: 'https://placeimg.com/140/140/any',
              },
            },
          ],
        })
      }
     
      onSend(messages = []) {
        this.setState(previousState => ({
          messages: GiftedChat.append(previousState.messages, messages),
        }))
      }
  
  
      renderAvatar(props) {
        return null;
      }
  
  
      renderInputToolbar(props){
        return null;
      }
    //Gifted
  
    render() {
        return (
                 <GiftedChat
            messages={this.state.messages}
            onSend={messages => this.onSend(messages)}
            user={{
              _id: 1,
            }}
          /> 
        )
    }
}











// import React, {Component } from 'react'
// import {View,FlatList } from 'react-native'
// import ContactListItem from "../components/ContactListItem";
// import Profile from "../screens/Profile";
// const keyExtractor=({id})=>id;
// export default class Contacts extends Component {
//     constructor(props) {
//         super(props);
//         console.log(this.props,"eeeeeeh");
//       }

//     contacts=[{id:'1',name:'Alaa Magdy',avatar:require('../assets/images/avatar1.jpg'),message:"Good morning Sir",time:'yesterday'},
//               {id:'2',name:'Yumna Magdy',avatar:require('../assets/images/avatar4.jpg'),message:"Good morning Sir",time:'8:28 pm'},
//               {id:'3',name:'Magdy Ibrahim',avatar:require('../assets/images/avatar3.jpg'),message:"Alaa!",time:'12 am'},
//               {id:'4',name:'Asmaa Eid',avatar:require('../assets/images/avatar2.jpg'),message:"How are You",time:'1 pm'},
//               {id:'5',name:'Yumna Magdy',avatar:require('../assets/images/avatar4.jpg'),message:"Good morning Sir",time:'8:28 pm'},
//               {id:'6',name:'Magdy Ibrahim',avatar:require('../assets/images/avatar3.jpg'),message:"Alaa!",time:'12 am'},
//               {id:'7',name:'Abdo Magdy',avatar:require('../assets/images/avatar5.jpg'),message:"Heeey are you here?",time:'5 am'},
//               {id:'8',name:'Menna Magdy',avatar:require('../assets/images/avatar6.jpg'),message:"Iwill travel to ..",time:'yesterday'}
//             ]
    
//               renderContact = ({ item }) => {
//                 const { id, name, avatar, message,time,onPress} = item;
//                 let {navigation}=this.props;

//                 return (
//                     <ContactListItem 
//                         name={name}
//                         avatar={avatar}
//                         message={message}
//                         time={time}
//                         onPress={()=>{navigation.navigate('Profile', { id })}}
//                     />
//                     ); };        
    
//      render() {
//         return (
//             <View>
//                <FlatList style={{backgroundColor:'white'}}
//                 data={this.contacts}
//                 renderItem={this.renderContact}
//                 keyExtractor={keyExtractor}/>
//             </View>
//        )
//     }
// }
