let express=require("express"),
speakerRouter=express.Router(),
path=require("path"),
mongoose=require("mongoose"),

fs=require("fs");
multer=require("multer");
multerMW=multer({
    dest:path.join(__dirname,"..","publics","images")
});


require("../Models/speakerModel");

        let speakerSchema=mongoose.model("speakers")

 speakerRouter.get("/list",(request,response)=>{
    
    speakerSchema.find({},(error,result)=>{
        if(!error)
        response.render("speakers/speakerslist",{speakers:result});

    });//list
    
    // response.send("LIST");
})


speakerRouter.get("/add",(request,response)=>{
    
    
    
    response.render("speakers/addspeaker");
})


speakerRouter.post("/add",multerMW.single("speakerImage"),(request,response)=>{
    fs.rename(request.file.path,path.join(request.file.destination,request.file.originalname),()=>{});
    // console.log(request.body);
    // console.log(request.file);
    // response.send(request.file);
    let speaker=new speakerSchema({
        _id:request.body.id,
       Name:request.body.name,
       image:request.file.originalname,
       jopTitle:request.body.joptitle,
       passWord:request.body.password,
       BirthDate:request.body.birthdate,
       Gender:request.body.gender
   });

    speaker.save((err)=>{


        if(!err)
        {
            // response.send("DONE")
            response.redirect("/speakers/list");
        }
        else
        {
            console.log(err);
        }
    })

})


speakerRouter.get("/edit/:id",(request,response)=>{
    speakerSchema.findOne({_id:request.params.id},(error,result)=>{
        response.render("speakers/editSpeaker",{speaker:result})

    });
})
speakerRouter.post("/edit/:id",multerMW.single("speakerImage"),(request,response)=>{
    fs.rename(request.file.path,path.join(request.file.destination,request.file.originalname),()=>{});

    speakerSchema.update({_id:request.params.id},{
        "$set":{
            
            Name:request.body.name,
            image:request.file.originalname,
            jopTitle:request.body.joptitle,
            passWord:request.body.password,
            BirthDate:request.body.birthdate,
            Gender:request.body.gender
           
           
        }
    },(error)=>{
        if(!error)
        {
            response.redirect("/speakers/list");
        }
    })


})
speakerRouter.get("/getById/:id",(request,response)=>{
    speakerSchema.findOne({_id:request.params.id},(error,result)=>{
        console.log(result);
        response.send(result);

    });
})
speakerRouter.get("/delete/:id",(request,response)=>{
    speakerSchema.deleteOne({_id:request.params.id},(error)=>{
        if(!error)
        response.redirect("/speakers/list");
    })
});

 
module.exports=speakerRouter;