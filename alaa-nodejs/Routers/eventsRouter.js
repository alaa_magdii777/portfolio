let express=require("express"),
eventRouter=express.Router(),
path=require("path"),
mongoose=require("mongoose");

require("../Models/speakerModel")
require("../Models/eventModel")

let eventSchema=mongoose.model("events");
let speakerSchema=mongoose.model("speakers");


eventRouter.get("/list",(request,response)=>{

    // eventSchema.find({},(error,result)=>{
    //     response.render("events/eventslist",{events:result});

    // })
    eventSchema.find({}).populate({"path":"mainSpeaker others"}).then((result)=>{
        console.log(result.others);
        response.render("events/eventslist",{events:result});

    })

});

eventRouter.get("/add",(request,response)=>{
    speakerSchema.find({},(error,result)=>{
        response.render("events/addevent",{speakers:result});
    })
})


eventRouter.post("/add",(request,response)=>{
    let event=new eventSchema({
        _id:request.body.id,
        title:request.body.name,
        mainSpeaker:request.body.mainSpeaker,
        others:request.body.otherSpeakers
    });
    event.save((error)=>{
        if(!error)
        response.redirect("/events/list");
    });
})



//edit events
eventRouter.get("/edit/:id",(request,response)=>{
    // eventSchema.findOne({_id:request.params.id},(error,result)=>{
    //     response.render("events/edit",{event:result});
    // })
    eventSchema.findOne({_id:request.params.id}).populate({"path":"mainSpeaker others"}).then((result)=>{
        speakerSchema.find({},(error,result2)=>{
            console.log(result.others);
            console.log(result2.others);
            response.render("events/editevent",{event:result,speakers:result2});

        })
            //response.render("events/edit",{event:result});

        
    })
});

eventRouter.post("/edit/:id",(request,response)=>{
    eventSchema.update({_id:request.params.id},{
        "$set":{
            title:request.body.title,
            mainSpeaker:request.body.mainSpeaker,
            others:request.body.otherSpeakers
        }
     },(error)=>{
         if(!error){
             response.redirect("/events/list");
         }
     }) 
    
});
//delete
eventRouter.get("/delete/:id",(request,response)=>{
    eventSchema.deleteOne({_id:request.params.id},(error)=>{
        if(!error)
        response.redirect("/events/list");
    })
})



module.exports=eventRouter;