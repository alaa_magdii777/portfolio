let mongoose=require("mongoose");
let AutoIncrement = require('mongoose-sequence')(mongoose);

let speakerSchema=new mongoose.Schema({
    _id:Number,
    Name:{
        type:String,
        required:true
        // default:
    },
    jopTitle:String,
    BirthDate:{type:Date},
    image:String,
   Gender:{enum:["M","F"]},
   
     passWord:Number,

    image:String
    // ,
    // Address:{
    //     city:String
    // }
});


speakerSchema.plugin(AutoIncrement, {inc_field: '_id'});

//mapping
mongoose.model("speakers",speakerSchema);
