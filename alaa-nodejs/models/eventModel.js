let mongoose=require("mongoose");

let eventSchema=new mongoose.Schema({
    _id:Number,
    title:String,
    mainSpeaker:{
        type:Number,
        ref:"speakers"
    },
    
    eventDate:{
        type:Date,
        default:Date.now 
    },
    
    others:[{
        type:Number,
        ref:"speakers"
    }]
});


mongoose.model("events",eventSchema);