import React, {Component} from 'react';
import axios from 'axios';
import {ScrollView, Image, FlatList} from 'react-native';
import {API_PREFIX} from './../shared/config';
import {
    Container, Header, Title, Content, Footer, FooterTab,
    Button, Left, Right, Body, Icon, Text, View,
    Card, CardItem, Thumbnail, Spinner, Drawer
} from 'native-base';
import {styles} from './../shared/styles';
import SideBar from './SideBar';

class CategoryComponent extends Component {

    constructor() {
        super();
        this.state = {
            categories: [],
            errors: [],
            isLoading: false,
        };
    }

    componentDidMount() {
        this.setState({isLoading: true});
        axios.get(API_PREFIX + "categories").then((response) => {
            //console.warn(response.data);
            const data = response.data;
            if (data['failed']) {
                this.setState({errors: data['errors']});
            } else {
                this.setState({categories: data['data']});
            }
        }).catch((error) => {
            console.warn(error);
        }).finally(() => {
            this.setState({isLoading: false});
        });
    }

    closeDrawer() {
        this.drawer._root.close();
    }

    openDrawer() {
        this.drawer._root.open();
    }

    render() {
        const catsJSX = (
            <FlatList
                data={this.state.categories}
                keyExtractor={(item) => {
                    return item['id'];
                }}
                renderItem={({item}) => {
                    return (
                        <Card>
                            <CardItem cardBody>
                                <Image source={{uri: item['imgURL']}}
                                       style={{height: 175, width: null, flex: 1}}/>

                                {/*<Image source={require('./../assets/images/g1.jpg')}*/}
                                {/*       style={{height: 175, width: null, flex: 1}}/>*/}

                            </CardItem>
                            <CardItem>
                                <Left>
                                    <Body style={styles.center}>
                                        <Text>{item['name']}</Text>
                                        <Text note>
                                            {item['description']}
                                        </Text>
                                    </Body>
                                </Left>
                            </CardItem>
                            <CardItem>
                                <Body style={styles.center}>
                                    <Button success rounded small onPress={() => {
                                        this.props.navigation.navigate('Products', {
                                            catID: item['id'],
                                        });
                                    }}>
                                        <Text>Show Products</Text>
                                    </Button>
                                </Body>
                            </CardItem>
                        </Card>
                    );
                }}
            />
        );

        return (
            <>
                <Drawer ref={(ref) => {
                    this.drawer = ref;
                }} content={<SideBar navigator={this.navigator}/>}
                        onClose={() => this.closeDrawer()}>
                    <Container>
                        <Header>
                            <Left>
                                <Button transparent onPress={() => {
                                    this.openDrawer();
                                }}>
                                    <Icon name='menu'/>
                                </Button>
                            </Left>
                            <Body>
                                <Title>Categories</Title>
                            </Body>
                            <Right/>
                        </Header>
                        <Content>
                            <ScrollView contentInsetAdjustmentBehavior="automatic">
                                {this.state.isLoading ? (
                                    <Spinner color={"red"}/>
                                ) : catsJSX}
                            </ScrollView>
                        </Content>
                    </Container>
                </Drawer>
            </>
        );
    }
}

export default CategoryComponent;
