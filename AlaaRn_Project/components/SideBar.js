import React, {Component} from 'react';
import {View, Text} from 'native-base';
import {styles} from "../shared/styles";

class SideBar extends Component {

    render() {
        return (
            <View style={[styles.whiteBG, {flex: 1}]}>
                <Text>Welcome</Text>
            </View>
        );
    }
}

export default SideBar;