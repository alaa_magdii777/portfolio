import React, {Component} from 'react';
import axios from 'axios';
import {styles} from './../shared/styles';

import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    FlatList,
    Alert,
    Image
} from 'react-native';

import {
    Container, Header, Title, Content, Button,
    Left, Right, Body, Icon, Footer, Form, View,Text,
    Input, Item, Card, CardItem, Thumbnail, Spinner
} from 'native-base';
import {API_PREFIX} from "../shared/config";

class ProductsComponent extends Component {

    constructor() {
        super();
        this.state = {
            name: "Selected Category",
            products: [],
            errors: [],
            isLoading: false,
        };
    }

    componentDidMount() {
        const catID = this.props.navigation.getParam('catID', null);
        //console.warn(catID);

        if (null) {
            this.props.navigation.navigate('Category');
        } else {
            this.setState({isLoading: true});
            // axios.get(API_PREFIX + "categories/" + catID).then((response) => {
            axios.get(API_PREFIX + "categories/" + catID).then((response) => {
                //console.warn(response.data);
                const data = response.data;
                if (data['failed']) {
                    this.setState({errors: data['errors']});
                } else {
                    this.setState({products: data['data']['products']});
                    this.setState({name: data['data']['name']});
                    //console.warn(this.state.name);
                }
            }).catch((error) => {
                console.warn(error.message);
            }).finally(() => {
                this.setState({isLoading: false})
            });
        }
    }

    render() {
        const productsJSX = (
            <FlatList
                data={this.state.products}
                keyExtractor={(item)=>{return item['id'];}}
                renderItem={({item})=> {
                    return(
                        <Card>
                            <CardItem cardBody>
                                {/*<Image source={require('./../assets/images/g1.jpg')}*/}
                                {/*       style={{height: 175, width: null, flex: 1}}/>*/}

                                <Image source={{uri: item['imgURL']}}
                                       style={{height: 175, width: null, flex: 1}}/>



                            </CardItem>
                            <CardItem>
                                <Left>
                                    <Body style={styles.center}>
                                        <Text>{item['name']}</Text>
                                        <Text note>
                                            {item['description']}
                                        </Text>
                                    </Body>
                                </Left>
                            </CardItem>
                            <CardItem>
                                <Body style={styles.center}>
                                    <Button success rounded small onPress={() => {
                                        // this.props.navigation.navigate('ProductDetails')
                                        // console.warn("iam here");

                                        this.props.navigation.navigate('ProductDetails', {
                                            proID: item['id'],
                                        });
                                    }}>
                                        <Text>Buy</Text>
                                    </Button>
                                </Body>
                            </CardItem>
                        </Card>
                    );
                }}
            />
        );

        return (
            <>
                <Container>
                    <Header>
                        <Left>
                            <Button transparent>
                                <Icon name='menu'/>
                            </Button>
                        </Left>
                        <Body>
                            <Title>{this.state.name}</Title>
                        </Body>
                        <Right/>
                    </Header>
                    <Content>
                        <ScrollView contentInsetAdjustmentBehavior="automatic">
                            {this.state.isLoading ? (
                                <Spinner color={"red"}/>
                            ): productsJSX}
                        </ScrollView>
                    </Content>
                </Container>
            </>
        );
    }
}

export default ProductsComponent;
