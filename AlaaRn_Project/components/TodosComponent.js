/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import axios from 'axios';

import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    FlatList,
    Alert,
    Image
} from 'react-native';

import {
    Container, Header, Title, Content, Button,
    Left, Right, Body, Text, Icon, Footer, Form, View,
    Input, Item, Card, CardItem, Thumbnail, Spinner
} from 'native-base';

class TodosComponent extends Component {

    constructor() {
        super();
        this.state = {
            todos: [],
            isLoading: false,
        };
    }

    componentDidMount() {
        this.setState({isLoading: true});
        //http://127.0.0.1/api/....
        //http://localhost/api/....
        axios.get('https://jsonplaceholder.typicode.com/todos').then((response) => {
            let data = response.data;
            this.setState({todos: data});
            //console.warn(data.length);
            this.setState({isLoading: false});
        }).catch(() => {

        });
    }

    render() {
        return (
            <>
                <Container>
                    <Header style={styles.bgPink}>
                        <Left>
                            <Button transparent>
                                <Icon name='menu'/>
                            </Button>
                        </Left>

                        <Body>
                            <Title style={styles.blackColor}>All Todos</Title>
                        </Body>
                        <Right/>
                    </Header>
                    <Content>
                        <SafeAreaView style={{flex: 1}}>
                            <ScrollView
                                contentInsetAdjustmentBehavior="automatic"
                                style={styles.scrollView}>

                                {
                                    this.state.isLoading ? (
                                        <View>
                                            <Spinner color='red'/>
                                        </View>
                                    ) : (
                                        <View>
                                            <FlatList
                                                data={this.state.todos}
                                                keyExtractor={(item) => {
                                                    return item.id.toString();
                                                }} renderItem={({item}) => {
                                                return (
                                                    <Card>
                                                        <CardItem>
                                                            <Left>
                                                                <Body>
                                                                    <Text>{item.title}</Text>
                                                                    <Text note>GeekyAnts</Text>
                                                                </Body>
                                                            </Left>
                                                            <Right>
                                                                {item['completed'] ? (
                                                                    <Icon type={"FontAwesome"} name={"thumbs-o-up"}/>
                                                                ) : (
                                                                    <Icon type={"FontAwesome"} name={"thumbs-o-down"}/>
                                                                )}
                                                            </Right>
                                                        </CardItem>
                                                    </Card>
                                                );
                                            }}/>
                                        </View>
                                    )
                                }
                            </ScrollView>
                        </SafeAreaView>
                    </Content>
                    <Footer style={styles.bgPink}/>
                </Container>
            </>
        );
    }
}

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: 'white',
    },
    marginForm: {marginRight: 20, marginLeft: 20, marginTop: 20, marginBottom: 20, backgroundColor: '#B0392B'},
    bgPink: {
        backgroundColor: '#1ec2ff',
        color: 'black',
    },
    blackColor: {
        color: 'black',
    },

});

export default TodosComponent;
