import React, {Component} from 'react';
import {FlatList, Image, ScrollView} from 'react-native';
import {
    Container,
    Header,
    Content,
    Card,
    CardItem,
    Thumbnail,
    Text,
    Button,
    Icon,
    Left,
    Body,
    Spinner
} from 'native-base';

import {styles} from './../shared/styles';
import axios from "axios";
import {API_PREFIX} from "../shared/config";

class ProductDetailsComponent extends Component {

    constructor() {
        super();
        this.state = {
            product: [],

        };
    }


    componentDidMount() {
        const proID = this.props.navigation.getParam('proID', null);

        //console.warn(proID);


        axios.get(API_PREFIX + "products/" + proID).then((response) => {
            console.warn(response.data.data);
            const data = response.data;
            if (data['failed']) {
                this.setState({errors: data['errors']});
            } else {
                this.setState({product: data['data']});
                //console.warn(this.state.products);
            }
        }).catch((error) => {
            console.warn(error);
        }).finally(() => {
            this.setState({isLoading: false});
        });
    }


    render() {


        const proJSX = (



            <FlatList
                data={[this.state.product]}

                keyExtractor={(item)=>{return item['id'];}}
                renderItem={({item})=> {
                    return(
                        <Card>
                            <CardItem cardBody>
                                {/*<Image source={require('./../assets/images/g1.jpg')}*/}
                                {/*       style={{height: 175, width: null, flex: 1}}/>*/}

                                <Image source={{uri: item['imgURL']}}
                                       style={{height: 175, width: null, flex: 1}}/>
                            </CardItem>
                            <CardItem>
                                <Left>
                                    <Body style={styles.center}>
                                        <Text>{item['name']}</Text>
                                        <Text note>
                                            {item['description']}
                                        </Text>

                                        <Text note>
                                            {item['buying_price']}
                                        </Text>

                                        <Text note>
                                            {item['selling_price']}
                                        </Text>



                                        <Text note>
                                            {item['discount']}
                                        </Text>
                                    </Body>
                                </Left>

                                {/*<Text>Alaa</Text>*/}
                            </CardItem>

                        </Card>
                    );
                }
                }
            />


        );


        return (
            <Container>
                <Header/>
                <Content>
                    <Card style={{flex: 0}}>


                        <CardItem>
                            {/*<Body>*/}
                            {/*    /!*<Image source={{uri: item['imgURL']}}*!/*/}
                            {/*    /!*       style={{height: 175, width: null, flex: 1}} />*!/*/}
                            {/*    <Text>*/}
                            {/*        Product Details*/}
                            {/*    </Text>*/}
                            {/*</Body>*/}

                            <Content>
                                <ScrollView contentInsetAdjustmentBehavior="automatic">
                                    {this.state.isLoading ? (
                                        <Spinner color={"red"}/>
                                    ) : proJSX}
                                </ScrollView>
                            </Content>

                        </CardItem>


                    </Card>
                </Content>
            </Container>
        );


    }
}


export default ProductDetailsComponent;
