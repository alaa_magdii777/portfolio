/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
// import TodosComponent from "./components/TodosComponent";
//import FirstComponent from "./components/FirstComponent";
import CategoryComponent from "./components/CategoryComponent";
import ProductsComponent from "./components/ProductsComponent";
import ProductDetailsComponent from "./components/ProductDetailsComponent";

// https://reactnavigation.org/docs/en/hello-react-navigation.html
const AppNavigator = createStackNavigator({
    // Todos: {
    //     screen: TodosComponent,
    // }, First: {
    //     screen: FirstComponent,
    // },

    Category: {
        screen: CategoryComponent,

    },

    Products: {
        screen: ProductsComponent,
    },

    ProductDetails: {
        screen: ProductDetailsComponent,

    },

    initialRouteName: 'Category',

// },
    // {
    // headerMode: 'none',
    // navigationOptions: {
    //     headerVisible: false,
    // }

});

export default createAppContainer(AppNavigator);
