    Abstract  
Academic Guidance:
The academic guidance system seeks to meet the needs of students and make services available to them to benefit from them.
The academic guidance system seeks to provide advice and help students so that they can successfully complete their school stage by achieving the 
  following objectives :-
�	To provide the necessary support to the student during his or her studies in order to complete the study plan and to terminate all requests within the time period available.
�	Providing academic and indicative information to students and increasing their awareness of the university's mission,
�	To pursue academic students and help them to complete their studies efficiently and to support the efforts of the University in providing a sound educational environment for the graduation of qualified students to the labour market.
�	To help students identify scientific disciplines that are suited to their intellectual abilities and inclinations.
