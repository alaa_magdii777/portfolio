﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSite.Admin
{
    public partial class Faculty : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                var context = new DAL.AcademicDataContext();

                var items = context.Faculties.ToList();

                gvData.DataSource = items;
                gvData.DataBind();

            }
            catch (Exception)
            {
            }
        }
    }
}