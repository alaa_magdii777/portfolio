﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.Master" AutoEventWireup="true" CodeBehind="AdminPanel.aspx.cs" Inherits="WebSite.Admin.AdminPanel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <title>Admin Panel</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <section id="subintro">
        <div class="jumbotron subhead" id="overview">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="centered">
                            <h3>Admin Panel</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb notop">
                        <li><a href="/defalt.aspx">Home</a><span class="divider">/</span></li>
                        <li class="active">Admin Panel</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="maincontent">
        <div class="container">
            <div class="row">
                <div class="span4">
                    <div class="project-widget">
                         <h4>
                            <i class="icon-48 icon-user"></i>
                            <a href="/Admin/Students.aspx">View Students</a>
                        </h4>
                    </div>
                </div>
                <div class="span4">
                    <div class="project-widget">
                         <h4>
                            <i class="icon-48 icon-comment"></i>
                            <a href="/Admin/ContactUsRequests.aspx">View Contact us Requests</a>
                        </h4>
                    </div>
                </div>
                 <div class="span4">
                    <div class="project-widget">
                         <h4>
                            <i class="icon-48 icon-comment"></i>
                            <a href="/Admin/Faculty.aspx">View Faculties</a>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>
