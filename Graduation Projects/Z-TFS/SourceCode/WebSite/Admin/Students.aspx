﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AdminMaster.Master" AutoEventWireup="true" CodeBehind="Students.aspx.cs" Inherits="WebSite.Admin.Students" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <title>Students</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <section id="subintro">
        <div class="jumbotron subhead" id="overview">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="centered">
                            <h3>View Students</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb notop">
                        <li><a href="/Admin/AdminPanel.aspx">Admin Panel</a><span class="divider">/</span></li>
                        <li class="active">Students</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="maincontent">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <asp:GridView runat="server" ID="gvData" AutoGenerateColumns="false" CssClass="table table-striped">
                        <Columns>
                            <asp:BoundField HeaderText="First Name" DataField="FirstName" />
                            <asp:BoundField HeaderText="Last Name" DataField="LastName" />
                            <asp:BoundField HeaderText="National ID" DataField="NationalID" />
                            <asp:BoundField HeaderText="Mobile" DataField="Mobile" />
                            <asp:BoundField HeaderText="Telephone" DataField="Telephone" />
                            <asp:BoundField HeaderText="High School Name" DataField="HighSchoolName" />
                            <asp:BoundField HeaderText="High School City" DataField="HighSchoolCity" />
                            <asp:BoundField HeaderText="Degree %" DataField="Degree" />
                            <asp:BoundField HeaderText="Faculty" DataField="Faculty" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
