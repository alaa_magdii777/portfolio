﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebSite.Admin.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <title>Admin Login</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <section id="subintro">
        <div class="jumbotron subhead" id="overview">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="centered">
                            <h3>Admin Login</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb notop">
                        <li><a href="/">Admin Panel</a><span class="divider">/</span></li>
                        <li class="active">Admin Login</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="maincontent">
        <div class="container">
            <div class="row">
                <div runat="server" id="divMessage"></div>
            </div>
            <div class="row">
                <div class="span12">
                    <div class="row">
                        <div class=" span12 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtEmail">Email</asp:Label>
                            <asp:TextBox runat="server" ID="txtEmail" CssClass=" span12 form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtEmail" runat="server" ValidationGroup="login"
                                ErrorMessage="Required Field" Text="Required Field" ControlToValidate="txtEmail" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                        <div class=" span12 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtPassword">Password</asp:Label>
                            <asp:TextBox runat="server" ID="txtPassword" CssClass=" span12 form-control" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtPassword" runat="server" ValidationGroup="login"
                                ErrorMessage="Required Field" Text="Required Field" ControlToValidate="txtPassword" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                        <div class=" span12 form-group text-center">
                            <asp:Button runat="server" ID="btnLogin" CssClass="btn btn-block btn-success"
                                ValidationGroup="login" Text="Login" OnClick="btnLogin_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
