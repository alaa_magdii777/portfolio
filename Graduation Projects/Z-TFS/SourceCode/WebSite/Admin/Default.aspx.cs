﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSite.Admin
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated && IsAdmin())
                {
                    Response.Redirect("/Admin/AdminPanel.aspx", false);

                }
            }
        }


        private bool IsAdmin()
        {
            return new BAL.Managers.UserManager().IsAdmin(Page.User.Identity.GetUserName());
        }


        protected void btnLogin_Click(object sender, EventArgs e)
        {
            var userStore = new UserStore<IdentityUser>();
            var manager = new UserManager<IdentityUser>(userStore);

            var loginUser = manager.Find(txtEmail.Text.Trim(), txtPassword.Text.Trim());

            if (loginUser != null)
            {
                if (manager.IsInRole(loginUser.Id, "Admin"))
                {
                    var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                    var userIdentity = manager.CreateIdentity(loginUser, DefaultAuthenticationTypes.ApplicationCookie);

                    authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, userIdentity);
                }
            }

            Response.Redirect("/Admin/AdminPanel.aspx", false);
        }


    }
}