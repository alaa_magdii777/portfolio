﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSite
{
    public partial class ContactUs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                var context = new DAL.AcademicDataContext();
                var contactUs = new DAL.ContactUs
                {
                    Name = txtName.Text.Trim(),
                    Email = txtEmail.Text.Trim(),
                    Subject = txtSubject.Text.Trim(),
                    Body = txtBody.Text.Trim(),
                    Date = DateTime.Now
                };

                context.ContactUs.InsertOnSubmit(contactUs);
                context.SubmitChanges();

                txtBody.Text = txtEmail.Text = txtName.Text = txtSubject.Text = string.Empty;

                sendmessage.Visible = true;
            }
            catch (Exception)
            {
                errormessage.Visible = true; 
            }
        }
    }
}