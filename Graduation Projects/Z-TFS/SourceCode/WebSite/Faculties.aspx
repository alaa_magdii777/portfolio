﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="Faculties.aspx.cs" Inherits="WebSite.Faculties" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <title> <%= Resources.Main.Faculties %>   </title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <section id="subintro">
        <div class="jumbotron subhead" id="overview">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="centered">
                            <h3><%= Resources.Main.Faculties %> </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb notop">
                        <li><a href="/"><%= Resources.Main.Home %> </a><span class="divider">/</span></li>
                        <li class="active"><%= Resources.Main.Faculties %> </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <ul class="portfolio-area da-thumbs">
                <li class="portfolio-item2" data-id="id-0" data-type="web">
                    <div class="span3">
                        <div class="thumbnail">
                            <div class="image-wrapp">
                                <img src="assets/img/dummies/1.jpeg" alt="" title="" />
                                <article class="da-animate da-slideFromRight" style="display: block;">
                                    <h4><%= Resources.Main.Faculty1 %>  </h4>
                                    <span><a class="zoom" data-pretty="prettyPhoto" href="assets/img/dummies/1.jpeg">
                                        <i class="icon-rounded icon-48 icon-zoom-in"></i>
                                    </a></span>
                                </article>
                            </div>
                        </div>
                        <div>
                           <h3> <a href="http://localhost:51320/Default.aspx"><%= Resources.Main.GoFacultyToSite %> </a></h3>
                        </div>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-0" data-type="web">
                    <div class="span3">
                        <div class="thumbnail">
                            <div class="image-wrapp">
                                <img src="assets/img/dummies/2.jpeg" alt="" title="" />
                                <article class="da-animate da-slideFromRight" style="display: block;">
                                    <h4><%= Resources.Main.Faculty2 %></h4>
                                    <span><a class="zoom" data-pretty="prettyPhoto" href="assets/img/dummies/2.jpeg">
                                        <i class="icon-rounded icon-48 icon-zoom-in"></i>
                                    </a></span>
                                </article>
                            </div>
                        </div>
                         <div>
                           <h3> <a href="#"><%= Resources.Main.GoFacultyToSite %> </a></h3>
                        </div>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-0" data-type="brand">
                    <div class="span3">
                        <div class="thumbnail">
                            <div class="image-wrapp">
                                <img src="assets/img/dummies/3.jpeg" alt="" title="" />
                                <article class="da-animate da-slideFromRight" style="display: block;">
                                    <h4><%= Resources.Main.Faculty3 %> </h4>
                                    <span><a class="zoom" data-pretty="prettyPhoto" href="assets/img/dummies/3.jpeg">
                                        <i class="icon-rounded icon-48 icon-zoom-in"></i>
                                    </a></span>
                                </article>
                            </div>
                        </div>
                         <div>
                           <h3> <a href="#"><%= Resources.Main.GoFacultyToSite %> </a></h3>
                        </div>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-0" data-type="photo">
                    <div class="span3">
                        <div class="thumbnail">
                            <div class="image-wrapp">
                                <img src="assets/img/dummies/4.jpeg" alt="" title="" />
                                <article class="da-animate da-slideFromRight" style="display: block;">
                                    <h4><%= Resources.Main.Faculty4 %> </h4>
                                    <span><a class="zoom" data-pretty="prettyPhoto" href="assets/img/dummies/4.jpeg">
                                        <i class="icon-rounded icon-48 icon-zoom-in"></i>
                                    </a></span>
                                </article>
                            </div>
                        </div>
                         <div>
                           <h3> <a href="#"><%= Resources.Main.GoFacultyToSite %> </a></h3>
                        </div>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-0" data-type="graphic">
                    <div class="span3">
                        <div class="thumbnail">
                            <div class="image-wrapp">
                                <img src="assets/img/dummies/5.jpeg" alt="" title="" />
                                <article class="da-animate da-slideFromRight" style="display: block;">
                                    <h4><%= Resources.Main.Faculty5 %> </h4>
                                    <span><a class="zoom" data-pretty="prettyPhoto" href="assets/img/dummies/5.jpeg">
                                        <i class="icon-rounded icon-48 icon-zoom-in"></i>
                                    </a></span>
                                </article>
                            </div>
                        </div>
                         <div>
                           <h3> <a href="#"><%= Resources.Main.GoFacultyToSite %> </a></h3>
                        </div>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-0" data-type="graphic">
                    <div class="span3">
                        <div class="thumbnail">
                            <div class="image-wrapp">
                                <img src="assets/img/dummies/6.jpeg" alt="" title="" />
                                <article class="da-animate da-slideFromRight" style="display: block;">
                                    <h4><%= Resources.Main.Faculty6 %> </h4>
                                    <span><a class="zoom" data-pretty="prettyPhoto" href="assets/img/dummies/6.jpeg">
                                        <i class="icon-rounded icon-48 icon-zoom-in"></i>
                                    </a></span>
                                </article>
                            </div>
                        </div>
                         <div>
                           <h3> <a href="#"><%= Resources.Main.GoFacultyToSite %> </a></h3>
                        </div>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-0" data-type="photo">
                    <div class="span3">
                        <div class="thumbnail">
                            <div class="image-wrapp">
                                <img src="assets/img/dummies/7.jpeg" alt="" title="" />
                                <article class="da-animate da-slideFromRight" style="display: block;">
                                    <h4><%= Resources.Main.Faculty7 %> </h4>
                                    <span><a class="zoom" data-pretty="prettyPhoto" href="assets/img/dummies/7.jpeg">
                                        <i class="icon-rounded icon-48 icon-zoom-in"></i>
                                    </a></span>
                                </article>
                            </div>
                        </div>
                         <div>
                           <h3> <a href="#"><%= Resources.Main.GoFacultyToSite %> </a></h3>
                        </div>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-0" data-type="ilustrator">
                    <div class="span3">
                        <div class="thumbnail">
                            <div class="image-wrapp">
                                <img src="assets/img/dummies/8.jpeg" alt="" title="" />
                                <article class="da-animate da-slideFromRight" style="display: block;">
                                    <h4><%= Resources.Main.Faculty8 %> </h4>
                                    <span><a class="zoom" data-pretty="prettyPhoto" href="assets/img/dummies/8.jpeg">
                                        <i class="icon-rounded icon-48 icon-zoom-in"></i>
                                    </a></span>
                                </article>
                            </div>
                        </div>
                         <div>
                           <h3> <a href="#"><%= Resources.Main.GoFacultyToSite %> </a></h3>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </section>
</asp:Content>

