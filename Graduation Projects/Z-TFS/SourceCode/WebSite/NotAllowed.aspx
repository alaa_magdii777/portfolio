﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="NotAllowed.aspx.cs" Inherits="WebSite.NotAllowed" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <title><%= Resources.Main.NotAllowed %></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <section id="subintro">
        <div class="jumbotron subhead" id="overview">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="centered">
                            <h3><%= Resources.Main.NotAllowed %></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb notop">
                        <li><a href="/"><%= Resources.Main.Home %></a><span class="divider">/</span></li>
                        <li class="active"><%= Resources.Main.NotAllowed %></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
                <div class="span12">
                    <div class="tagline centered">
                        <div class="row" id="divRegister" runat="server">
                            <div class="span12">
                                <div class="tagline_text">
                                    <h3><%= Resources.Main.NotAllowedMessage %></h3>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                    <!-- end tagline -->
                </div>
            </div>
        
    </section>
</asp:Content>
