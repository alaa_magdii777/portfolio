﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.BAL.Managers
{
    public class UserManager
    {
        public DAL.UserInfo GetUserInformation()
        {
            try
            {
                
                var context = new DAL.AcademicDataContext();

                var currentUser = context.AspNetUsers.FirstOrDefault(a => a.UserName.ToLower() == HttpContext.Current.User.Identity.Name.ToLower()); 

                if(currentUser != null)
                {
                    return context.UserInfos.FirstOrDefault(u => u.UserId == currentUser.Id); 
                }
                return null; 
            }
            catch (Exception)
            {
                return null; 
            }
        }

        public string GetCurrentUserName()
        {
            var user = GetUserInformation(); 

            if(user != null )
            {
                return string.Format("{0} {1}", user.FirstName, user.LastName); 
            }

            return string.Empty;
        }


        public bool IsAdmin(string userId)
        {
            try
            {
                var context = new DAL.AcademicDataContext();

                var currentUser = context.AspNetUsers.FirstOrDefault(a => a.UserName.ToLower() == HttpContext.Current.User.Identity.Name.ToLower());

                if (currentUser != null)
                {
                    return context.AspNetUserRoles.Any(u => u.UserId == currentUser.Id && u.RoleId == "1" );
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}