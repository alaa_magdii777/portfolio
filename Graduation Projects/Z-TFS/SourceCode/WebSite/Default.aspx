﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebSite.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <title><%= Resources.Main.Home %></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <section id="maincontent">
        <div class="container">
            <div class="row">
                <div class="span12"> 

                    <div class="flexslider">
                        <ul class="slides">
                            <li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; display: list-item;">
                                <img style="max-height: 450px !important" src="/assets/img/slides/flexslider/8.jpg" alt="">
                            </li>
                            <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;" class="">
                                <img style="max-height: 450px !important" src="/assets/img/slides/flexslider/9.jpeg" alt="">
                            </li>
                            <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;" class="">
                                <img style="max-height: 450px !important" src="/assets/img/slides/flexslider/img6.jpg" alt="">
                            </li>
                        </ul>

                        <ul class="flex-direction-nav">
                            <li><a class="flex-prev" href="#">Previous</a></li>
                            <li><a class="flex-next" href="#">Next</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="span12">
                    <div class="tagline centered">
                        <div class="row" id="divRegister" runat="server">
                            <div class="span12">
                                <div class="tagline_text">
                                    <h2><%= Resources.Main.NoAccount %></h2>
                                </div>
                                <div class="btn-toolbar cta">
                                    <a class="btn btn-large btn-color" href="/Register.aspx">
                                        <i class="icon-user icon-white"></i><%= Resources.Main.Register %> </a>
                                    <a class="btn btn-large btn-inverse" href="/Login.aspx">
                                        <i class="icon-lock icon-white"></i><%= Resources.Main.SignIn %></a>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="divLoggedInUser" runat="server">
                            <div class="span12">
                                <div class="tagline_text">
                                    <h2>
                                        <asp:Label runat="server" ID="lblUsername">
                                        </asp:Label>
                                    </h2>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <!-- end tagline -->
                </div>
            </div>
            <div class="row">
                <div class="span4">
                    <div class="well">
                        <div class="centered">
                            <i class="icon-circled icon-64 icon-ok-sign active"></i>
                            <h4><%= Resources.Main.OurValues %></h4>
                            <div class="dotted_line">
                            </div>
                            <p>
                                <%= Resources.Main.OurValues2 %>                                
                            </p>
                            <span class="emphasis-2"><a class="btn btn-success" href="/OurValues.aspx"> <%= Resources.Main.ReadMore %> </a></span>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="well">
                        <div class="centered">
                            <i class="icon-circled icon-64 icon-envelope active"></i>
                            <h4><%= Resources.Main.OurMission %></h4>
                            <div class="dotted_line">
                            </div>
                            <p>
                                <%= Resources.Main.OurMission2 %>                                
                            </p>
                            <span class="emphasis-2"><a class="btn btn-success" href="/Mission.aspx"> <%= Resources.Main.ReadMore %> </a></span>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="well">
                        <div class="centered">
                            <i class="icon-circled icon-64  icon-comment active"></i>
                            <h4><%= Resources.Main.OurVision %></h4>
                            <div class="dotted_line">
                            </div>
                            <p>
                                <%= Resources.Main.OurVision2 %>                               
                            </p>
                            <span class="emphasis-2"><a class="btn btn-success" href="/Vision.aspx"> <%= Resources.Main.ReadMore %>  </a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="home-posts">
                    <div class="span12">
                        <h3><%= Resources.Main.RecentBlogPosts %></h3>
                    </div>
                    <div class="span3">
                        <div class="post-image">
                            <a href="#">
                                <img src="assets/img/dummies/blog1.jpg" alt="">
                            </a>
                        </div>
                        <div class="entry-meta">
                            <a href="#"><i class="icon-square icon-48 icon-pencil left"></i></a>
                        </div>
                        <!-- end .entry-meta -->
                        <div class="entry-body">
                            <a href="#">
                                <h5 class="title"><%= Resources.Main.Post1 %></h5>
                            </a>
                            <p>
                                <%= Resources.Main.PostContent1 %>
			 
                            </p>
                        </div>
                        <!-- end .entry-body -->
                        <div class="clear">
                        </div>
                    </div>
                    <div class="span3">
                        <div class="post-image">
                            <a href="#">
                                <img src="assets/img/dummies/blog2.jpg" alt=""></a>
                        </div>
                        <div class="entry-meta">
                            <a href="#"><i class="icon-square icon-48 icon-pencil left"></i></a>
                        </div>
                        <!-- end .entry-meta -->
                        <div class="entry-body">
                            <a href="#">
                                <h5 class="title"><%= Resources.Main.Post2 %></h5>
                            </a>
                            <p>
                               <%= Resources.Main.PostContent2 %>
			 
                            </p>
                        </div>
                        <!-- end .entry-body -->
                        <div class="clear">
                        </div>
                    </div>
                    <div class="span3">
                        <div class="post-image">
                            <a href="#">
                                <img src="assets/img/dummies/blog1.jpg" alt="">
                            </a>
                        </div>
                        <div class="entry-meta">
                            <a href="#"><i class="icon-square icon-48 icon-pencil left"></i></a>
                        </div>
                        <!-- end .entry-meta -->
                        <div class="entry-body">
                            <a href="#">
                                <h5 class="title"><%= Resources.Main.Post3 %></h5>
                            </a>
                            <p>
                                <%= Resources.Main.PostContent3 %>
                            </p>
                        </div>
                        <!-- end .entry-body -->
                        <div class="clear">
                        </div>
                    </div>
                    <div class="span3">
                        <div class="post-image">
                            <a href="#">
                                <img src="assets/img/dummies/blog2.jpg" alt=""></a>
                        </div>
                        <div class="entry-meta">
                            <a href="#"><i class="icon-square icon-48 icon-pencil left"></i></a>
                        </div>
                        <!-- end .entry-meta -->
                        <div class="entry-body">
                            <a href="#">
                                <h5 class="title"><%= Resources.Main.Post4 %></h5>
                            </a>
                            <p>
                               <%= Resources.Main.PostContent4 %>
                            </p>
                        </div>
                        <!-- end .entry-body -->
                        <div class="clear">
                        </div>
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </section>
</asp:Content>
