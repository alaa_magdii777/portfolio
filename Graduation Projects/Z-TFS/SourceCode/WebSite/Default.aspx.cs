﻿using System;
using System.Web;

namespace WebSite
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                divLoggedInUser.Visible = true;
                divRegister.Visible = false;
                lblUsername.Text = string.Format("Welcome back {0} ", new BAL.Managers.UserManager().GetCurrentUserName()); 
            }
            else
            {
                divLoggedInUser.Visible = false;
                divRegister.Visible = true;
            }
        }
    }
}