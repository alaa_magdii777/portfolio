﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="Colleges.aspx.cs" Inherits="WebSite.Colleges" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <title>
         <%= Resources.Main.Colleges %>
    </title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
     <section id="subintro">
        <div class="jumbotron subhead" id="overview">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="centered">
                            <h3> <%= Resources.Main.Colleges %></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb notop">
                        <li><a href="/">Home <%= Resources.Main.Home %>a><span class="divider">/</span></li>
                        <li class="active"> <%= Resources.Main.Colleges %></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section>
         <%= Resources.Main.DummyContents %>
    </section>
</asp:Content>
