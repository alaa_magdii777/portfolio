﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="Vision.aspx.cs" Inherits="WebSite.Vision" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <title>
       <%= Resources.Main.OurVision %>
    </title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <section id="subintro">
        <div class="jumbotron subhead" id="overview">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="centered">
                            <h3><%= Resources.Main.OurVision %></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb notop">
                        <li><a href="/"><%= Resources.Main.Home %></a><span class="divider">/</span></li>
                        <li class="active"><%= Resources.Main.OurVision %></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section>
      <%= Resources.Main.dummyvision %>
    </section>
</asp:Content>
