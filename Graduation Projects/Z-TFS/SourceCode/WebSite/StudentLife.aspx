﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="StudentLife.aspx.cs" Inherits="WebSite.StudentLife" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <title><%= Resources.Main.StudentLife %></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <section id="subintro">
        <div class="jumbotron subhead" id="overview">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="centered">
                            <h3><%= Resources.Main.StudentLife %></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb notop">
                        <li><a href="/"><%= Resources.Main.Home %></a><span class="divider">/</span></li>
                        <li class="active"><%= Resources.Main.StudentLife %></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section>
       <div class="row">
        <ul class="portfolio-area da-thumbs">
          <li class="portfolio-item2" data-id="id-0" data-type="web">
            <div class="span3">
              <div class="thumbnail">
                <div class="image-wrapp">
                  <img src="assets/img/dummies/18.jpg" alt="" title="" />
                  <article class="da-animate da-slideFromRight" style="display: block;">
                    <h4><%= Resources.Main.StudentLife %></h4>                    
                    <span><a class="zoom" data-pretty="prettyPhoto" href="assets/img/dummies/18.jpg">
						<i class="icon-rounded icon-48 icon-zoom-in"></i>
						</a></span>
                  </article>
                </div>
              </div>
            </div>
          </li>
          <li class="portfolio-item2" data-id="id-0" data-type="web">
            <div class="span3">
              <div class="thumbnail">
                <div class="image-wrapp">
                  <img src="assets/img/dummies/11.jpg" alt="" title="" />
                  <article class="da-animate da-slideFromRight" style="display: block;">
                    <h4><%= Resources.Main.StudentLife %></h4>                   
                    <span><a class="zoom" data-pretty="prettyPhoto" href="assets/img/dummies/11.jpg">
						<i class="icon-rounded icon-48 icon-zoom-in"></i>
						</a></span>
                  </article>
                </div>
              </div>
            </div>
          </li>
          <li class="portfolio-item2" data-id="id-0" data-type="brand">
            <div class="span3">
              <div class="thumbnail">
                <div class="image-wrapp">
                  <img src="assets/img/dummies/16.jpg" alt="" title="" />
                  <article class="da-animate da-slideFromRight" style="display: block;">
                    <h4><%= Resources.Main.StudentLife %></h4>
                    <span><a class="zoom" data-pretty="prettyPhoto" href="assets/img/dummies/16.jpg">
						<i class="icon-rounded icon-48 icon-zoom-in"></i>
						</a></span>
                  </article>
                </div>
              </div>
            </div>
          </li>
          <li class="portfolio-item2" data-id="id-0" data-type="photo">
            <div class="span3">
              <div class="thumbnail">
                <div class="image-wrapp">
                  <img src="assets/img/dummies/12.jpeg" alt="" title="" />
                  <article class="da-animate da-slideFromRight" style="display: block;">
                    <h4><%= Resources.Main.StudentLife %></h4>
                    <span><a class="zoom" data-pretty="prettyPhoto" href="assets/img/dummies/12.jpeg">
						<i class="icon-rounded icon-48 icon-zoom-in"></i>
						</a></span>
                  </article>
                </div>
              </div>
            </div>
          </li>
          <li class="portfolio-item2" data-id="id-0" data-type="graphic">
            <div class="span3">
              <div class="thumbnail">
                <div class="image-wrapp">
                  <img src="assets/img/dummies/13.jpeg" alt="" title="" />
                  <article class="da-animate da-slideFromRight" style="display: block;">
                    <h4><%= Resources.Main.StudentLife %></h4>
                    <span><a class="zoom" data-pretty="prettyPhoto" href="assets/img/dummies/13.jpeg">
						<i class="icon-rounded icon-48 icon-zoom-in"></i>
						</a></span>
                  </article>
                </div>
              </div>
            </div>
          </li>
          <li class="portfolio-item2" data-id="id-0" data-type="graphic">
            <div class="span3">
              <div class="thumbnail">
                <div class="image-wrapp">
                  <img src="assets/img/dummies/14.jpeg" alt="" title="" />
                  <article class="da-animate da-slideFromRight" style="display: block;">
                    <h4><%= Resources.Main.StudentLife %></h4>
                    <span><a class="zoom" data-pretty="prettyPhoto" href="assets/img/dummies/14.jpeg">
						<i class="icon-rounded icon-48 icon-zoom-in"></i>
						</a></span>
                  </article>
                </div>
              </div>
            </div>
          </li>
          <li class="portfolio-item2" data-id="id-0" data-type="photo">
            <div class="span3">
              <div class="thumbnail">
                <div class="image-wrapp">
                  <img src="assets/img/dummies/15.jpeg" alt="" title="" />
                  <article class="da-animate da-slideFromRight" style="display: block;">
                    <h4><%= Resources.Main.StudentLife %></h4>
                    <span><a class="zoom" data-pretty="prettyPhoto" href="assets/img/dummies/15.jpeg">
						<i class="icon-rounded icon-48 icon-zoom-in"></i>
						</a></span>
                  </article>
                </div>
              </div>
            </div>
          </li>
          <li class="portfolio-item2" data-id="id-0" data-type="ilustrator">
            <div class="span3">
              <div class="thumbnail">
                <div class="image-wrapp">
                  <img src="assets/img/dummies/17.jpeg" alt="" title="" />
                  <article class="da-animate da-slideFromRight" style="display: block;">
                    <h4><%= Resources.Main.StudentLife %></h4>
                    <span><a class="zoom" data-pretty="prettyPhoto" href="assets/img/dummies/17.jpeg">
						<i class="icon-rounded icon-48 icon-zoom-in"></i>
						</a></span>
                  </article>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </section>
</asp:Content>
