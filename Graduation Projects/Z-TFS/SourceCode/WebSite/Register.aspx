﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="WebSite.Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <title><%= Resources.Main.ApplyRegister %> </title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <section id="subintro">
        <div class="jumbotron subhead" id="overview">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="centered">
                            <h3><%= Resources.Main.ApplyRegister %> </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb notop">
                        <li><a href="/"><%= Resources.Main.Home %> </a><span class="divider">/</span></li>
                        <li class="active"><%= Resources.Main.ApplyRegister %> </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="maincontent">
        <div class="container">
            <div class="row">
                <div runat="server" id="divMessage"></div>
            </div>
            <div class="row">
                <div class="span12">
                    <div class="row">
                        <div class=" span12 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtEmail"><%= Resources.Main.Email %>  <span style="color:red">*</span></asp:Label>
                            <asp:TextBox runat="server" ID="txtEmail" CssClass=" span12 form-control" TextMode="Email"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtEmail" runat="server" ValidationGroup="Register"
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="txtEmail" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                        <div class=" span12 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtPassword"><%= Resources.Main.Password %> <span style="color:red">*</span></asp:Label>
                            <asp:TextBox runat="server" ID="txtPassword" CssClass=" span12 form-control" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtPassword" runat="server" ValidationGroup="Register"
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="txtPassword" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                        <div class=" span12 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtConfirmPassword"><%= Resources.Main.ConfirmPassword %><span style="color:red">*</span></asp:Label>
                            <asp:TextBox runat="server" ID="txtConfirmPassword" CssClass=" span12 form-control" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtConfirmPassword" runat="server" ValidationGroup="Register"
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="txtConfirmPassword" CssClass="validation"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvtxtConfirmPassword" runat="server" ErrorMessage="Password doesn't mach"
                                ValidationGroup="Register" ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword"
                                Text="Password doesn't mach" CssClass="validation">
                            </asp:CompareValidator>
                        </div>
                        <div class=" span12 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtFirstName"><%= Resources.Main.FirstName %><span style="color:red">*</span></asp:Label>
                            <asp:TextBox runat="server" ID="txtFirstName" CssClass=" span12 form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtFirstName" runat="server" ValidationGroup="Register"
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="txtFirstName" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                        <div class=" span12 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtLastName"><%= Resources.Main.LastName %><span style="color:red">*</span></asp:Label>
                            <asp:TextBox runat="server" ID="txtLastName" CssClass=" span12 form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtLastName" runat="server" ValidationGroup="Register"
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="txtLastName" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                        <div class=" span12 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtNationalId"><%= Resources.Main.Gender %>  <span style="color:red">*</span></asp:Label>
                            <asp:RadioButtonList runat="server" ID="rdlGender" CssClass=" span12 radio ">
                                <asp:ListItem Text="<%$ Resources:Main, Male %>" Value="1"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Main, Female %>" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Register" InitialValue="-1"
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="rdlGender" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                        <div class=" span12 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtNationalId"><%= Resources.Main.NationalId %><span style="color:red">*</span></asp:Label>
                            <asp:TextBox runat="server" ID="txtNationalId" CssClass=" span12 form-control" onkeypress="return isNumber(event)"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtNationalId" runat="server" ValidationGroup="Register"
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="txtNationalId" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                        <div class=" span12 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtMobile"><%= Resources.Main.Mobile %> <span style="color:red">*</span></asp:Label>
                            <asp:TextBox runat="server" ID="txtMobile" CssClass=" span12 form-control" onkeypress="return isNumber(event)"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtMobile" runat="server" ValidationGroup="Register"
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="txtMobile" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                        <div class=" span12 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtPhone"><%= Resources.Main.Telephone %> <span style="color:red">*</span></asp:Label>
                            <asp:TextBox runat="server" ID="txtPhone" CssClass=" span12 form-control" onkeypress="return isNumber(event)"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtPhone" runat="server" ValidationGroup="Register"
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="txtPhone" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                        <div class=" span12 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtHighSchoolNome"><%= Resources.Main.HSN %> <span style="color:red">*</span></asp:Label>
                            <asp:TextBox runat="server" ID="txtHighSchoolNome" CssClass=" span12 form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtHighSchoolNome" runat="server" ValidationGroup="Register"
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="txtHighSchoolNome" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                        <div class=" span12 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtHighSchoolCity"> <%= Resources.Main.HSC %> <span style="color:red">*</span></asp:Label>
                            <asp:TextBox runat="server" ID="txtHighSchoolCity" CssClass=" span12 form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtHighSchoolCity" runat="server" ValidationGroup="Register"
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="txtHighSchoolCity" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                        <div class=" span12 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtDegree"><%= Resources.Main.Degree %> <span style="color:red">*</span></asp:Label>
                            <asp:TextBox runat="server" ID="txtDegree" CssClass=" span12 form-control" TextMode="Number" MaxLength="3"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtDegree" runat="server" ValidationGroup="Register"
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="txtDegree" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                        <div class=" span12 form-group text-center">
                            <p>
                                <asp:Button runat="server" ID="btnRegister" CssClass="btn btn-block btn-success"
                                    ValidationGroup="Register" Text="<%$ Resources:Main, ApplyRegister %>" OnClick="btnRegister_Click" />
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>

</asp:Content>
