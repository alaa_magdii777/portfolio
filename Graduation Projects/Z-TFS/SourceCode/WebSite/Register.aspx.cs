﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Web;

namespace WebSite
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("/default.aspx", false);
            }
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            var userStore = new UserStore<IdentityUser>();
            var manager = new UserManager<IdentityUser>(userStore);

            var user = new IdentityUser() { UserName = txtEmail.Text.Trim() };
            IdentityResult result = manager.Create(user, txtPassword.Text.Trim());


            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());
            roleManager.Create(new IdentityRole("Student"));
            manager.AddToRole(user.Id, "Student");

            if (result.Succeeded)
            {

                var context = new DAL.AcademicDataContext();
                var userInfo = new DAL.UserInfo
                {
                    Id= Guid.NewGuid().ToString(),
                    UserId = user.Id,
                    FirstName = txtFirstName.Text.Trim(),
                    LastName = txtLastName.Text.Trim(),
                    NationalID = txtNationalId.Text.Trim(),
                    IsMale = rdlGender.SelectedValue == "1" ? true : false,
                    Mobile = txtMobile.Text.Trim(),
                    Telephone = txtPhone.Text.Trim(),
                    HighSchoolName = txtHighSchoolNome.Text.Trim(),
                    HighSchoolCity = txtHighSchoolCity.Text.Trim(),
                    Degree = txtDegree.Text.Trim()
                };

                context.UserInfos.InsertOnSubmit(userInfo);
                context.SubmitChanges(); 

                var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                var userIdentity = manager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);

                authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, userIdentity);


                Response.Redirect("/FacultyFilter.aspx", false);
            }
            else
            {
                divMessage.InnerText = "Something went wrong, please try again later.";
            }
        }
    }
}