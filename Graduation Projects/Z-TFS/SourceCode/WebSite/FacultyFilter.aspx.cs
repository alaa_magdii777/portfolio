﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSite
{
    public partial class FacultyFilter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }

        private void BindData()
        {
            try
            {
                var userInfo = new BAL.Managers.UserManager().GetUserInformation();

                var degree = int.Parse(userInfo.Degree);

                var context = new DAL.AcademicDataContext();
                var items = context.Faculties.Where(f => f.MinPercentage <= degree).ToList();

                rblFaculty.DataSource = items;
                rblFaculty.DataTextField = "FacultyName";
                rblFaculty.DataValueField = "Id"; 
                rblFaculty.DataBind();
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                var context = new DAL.AcademicDataContext();

                var item = new DAL.UserFaculty()
                {
                    FacultyId = int.Parse(rblFaculty.SelectedValue),
                    UserId = new BAL.Managers.UserManager().GetUserInformation().UserId,


                };

                context.UserFaculties.InsertOnSubmit(item); 
                context.SubmitChanges();

                Response.Redirect("/", false);
            }
            catch (Exception ex)
            {
            }
        }
    }
}