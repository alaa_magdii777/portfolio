﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="Admissions.aspx.cs" Inherits="WebSite.Admissions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <%= Resources.Main.Admissions %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <section id="subintro">
        <div class="jumbotron subhead" id="overview">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="centered">
                            <h3><%= Resources.Main.Admissions %></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb notop">
                        <li><a href="/"><%= Resources.Main.Home %></a><span class="divider">/</span></li>
                        <li class="active"><%= Resources.Main.Admissions %></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section>

        <div class="row">
            <div class="span12">

                <div class="flexslider">
                    <ul class="slides">
                        <li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; display: list-item;">
                            <img style="max-height: 450px !important" src="/assets/img/slides/flexslider/h.jpeg" alt="">
                        </li>
                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;" class="">
                            <img style="max-height: 450px !important" src="/assets/img/slides/flexslider/sh.jpeg" alt="">
                        </li>
                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;" class="">
                            <img style="max-height: 450px !important" src="/assets/img/slides/flexslider/shi.jpeg" alt="">
                        </li>
                    </ul>

                    <ul class="flex-direction-nav">
                        <li><a class="flex-prev" href="#">Previous</a></li>
                        <li><a class="flex-next" href="#">Next</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="span12">
                <div class="tagline ">
                    <div class="row">
                        <div class="span12">
                            <div class="tagline_text">
                                <h2><%= Resources.Main.HowToApply %></h2>
                                <div>
                                    <%= Resources.Main.HowToApplyContents %>
                                </div>
                            </div>
                            <div class="btn-toolbar cta centered">
                                <a class="btn btn-large btn-success" href="/Register.aspx" runat="server" id="anchorApply">
                                    <i class="icon-plane icon-white"></i><%= Resources.Main.ApplyNow %> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</asp:Content>
