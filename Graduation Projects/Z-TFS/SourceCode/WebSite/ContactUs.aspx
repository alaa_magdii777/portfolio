﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="WebSite.ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <title> <%= Resources.Main.ContactUs %></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <section id="subintro">
        <div class="jumbotron subhead" id="overview">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="centered">
                            <h3> <%= Resources.Main.ContactUs %></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb notop">
                        <li><a href="/"> <%= Resources.Main.Home %></a><span class="divider">/</span></li>
                        <li class="active"> <%= Resources.Main.ContactUs %></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="maincontent">
        <div class="container">
            <div class="row">
                <div class="span4">
                    <aside>
                        <div class="widget">
                            <h4> <%= Resources.Main.GetInTouch %></h4>
                            <ul>
                                <li>
                                    <label><strong><%= Resources.Main.Phone %> </strong></label>
                                    <p>
                                        <%= Resources.Main.Phone2 %>              
                                    </p>
                                </li>
                                <li>
                                    <label><strong> <%= Resources.Main.Email %>    </strong></label>
                                    <p>
                                        <%= Resources.Main.Email2 %> 
                 
                                    </p>
                                </li>
                                <li>
                                    <label><strong><%= Resources.Main.Address %> </strong></label>
                                    <p>
                                      <%= Resources.Main.Address2 %> 
                 
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <div class="widget">
                            <h4><%= Resources.Main.SocialNetwork %> </h4>
                            <ul class="social-links">
                                <li><a href="#" title="Twitter"><i class="icon-rounded icon-32 icon-twitter"></i></a></li>
                                <li><a href="#" title="Facebook"><i class="icon-rounded icon-32 icon-facebook"></i></a></li>
                                <li><a href="#" title="Google plus"><i class="icon-rounded icon-32 icon-google-plus"></i></a></li>
                                <li><a href="#" title="Linkedin"><i class="icon-rounded icon-32 icon-linkedin"></i></a></li>
                                <li><a href="#" title="Pinterest"><i class="icon-rounded icon-32 icon-pinterest"></i></a></li>
                            </ul>
                        </div>
                    </aside>
                </div>
                <div class="span8">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13673.77425404481!2d31.34962251881877!3d31.04174911887397!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14f79dd4295c80e9%3A0x29c566a018cecb77!2sMansoura+University!5e0!3m2!1sen!2sus!4v1524650149076" width="750" height="450" frameborder="0" style="border: 0" allowfullscreen></iframe>

                    <div class="spacer30"></div>

                    <div runat="server" class="SuccessMessage" id="sendmessage" visible="false"><%= Resources.Main.ContactUsSuccessMessage %></div>
                    <div runat="server" class="ErrorMessage" id="errormessage" visible="false"><%= Resources.Main.ContactUsErrorMessage %></div>
                    <div class="row">
                        <div class="span4 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtName"><%= Resources.Main.Name %></asp:Label>
                            <asp:TextBox runat="server" ID="txtName" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtName" runat="server" ValidationGroup="Contact" 
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="txtName"  CssClass="validation"></asp:RequiredFieldValidator>
                        </div>

                        <div class="span4 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtEmail"><%= Resources.Main.Email %></asp:Label>
                            <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" TextMode="Email"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtEmail" runat="server" ValidationGroup="Contact"
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="txtEmail" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                        <div class="span8 form-group">
                            <asp:Label runat="server" AssociatedControlID="txtSubject"><%= Resources.Main.Subject %></asp:Label>
                            <asp:TextBox runat="server" ID="txtSubject" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Contact"
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="txtSubject" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                        <div class="span8 form-group">
                             <asp:Label runat="server" AssociatedControlID="txtSubject"><%= Resources.Main.Body %></asp:Label>
                            <asp:TextBox runat="server" ID="txtBody" CssClass="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Contact"
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="txtBody" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                         <div class=" span8 form-group text-center">
                            <asp:Button runat="server" ID="btnSend" CssClass="btn btn-color btn-rounded"
                                ValidationGroup="Contact" Text="<%$ Resources:Main, Send %>" OnClick="btnSend_Click" />
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</asp:Content>
