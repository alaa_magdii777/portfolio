﻿<%@ Master Language="C#" AutoEventWireup="true" CodeBehind="MainMaster.master.cs" Inherits="WebSite.MasterPages.MainMaster" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title>Academic Advising</title>
    <asp:ContentPlaceHolder ID="headContentPlaceHolder" runat="server">
    </asp:ContentPlaceHolder>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- styles -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,600,700" rel="stylesheet">
    <link href="/Assets/css/bootstrap.css" rel="stylesheet">
    <link href="/Assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="/Assets/css/docs.css" rel="stylesheet">
    <link href="/Assets/css/prettyPhoto.css" rel="stylesheet">
    <link href="/Assets/js/google-code-prettify/prettify.css" rel="stylesheet">
    <link href="/Assets/css/flexslider.css" rel="stylesheet">
    <link href="/Assets/css/sequence.css" rel="stylesheet">

    <link id="MyStyleSheet" rel="stylesheet" type="text/css" runat="server" />

    <link href="/Assets/color/default.css" rel="stylesheet">

    <!-- fav and touch icons -->
    <link rel="shortcut icon" href="/Assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/Assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/Assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/Assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/Assets/ico/apple-touch-icon-57-precomposed.png">

    <script type="text/javascript">!function (t, e) {
            "use strict"; var r = function (t) {
                try {
                    var r = e.head || e.getElementsByTagName("head")[0], a = e.createElement("script"); a.setAttribute("type", "text/javascript"),
                        a.setAttribute("src", t), r.appendChild(a)
                } catch (t) { }
            }; t.CollectId = "5b2702e9f24153360d3c3fc1", r("https://collectcdn.com/launcher.js")
        }(window, document);

    </script>
</head>
<body>

    <form runat="server">

        <header>
            <div class="navbar navbar-fixed-top">
                <div class="navbar-inner">
                    <div class="container">
                        <a class="brand logo" href="/">
                            <img runat="server" src="<%$ Resources:Main, LogoUrl %>" alt="">
                        </a>
                        <div class="navigation">
                            <nav>
                                <ul class="nav topnav">
                                    <li class="dropdown active">
                                        <a href="/"><%= Resources.Main.Home%></a>
                                    </li>
                                    <li class="dropdown active">
                                        <a href="/AboutUs.aspx"><%= Resources.Main.AboutUs %></a>
                                    </li>
                                    <li class="dropdown active">
                                        <a href="/Admissions.aspx"><%= Resources.Main.Admissions %></a>
                                        <ul runat="server" id="ulLogin" class="dropdown-menu" style="display: none;">
                                            <li><a href="/Register.aspx"><%= Resources.Main.Register %></a></li>
                                            <li><a href="/Login.aspx"><%= Resources.Main.Login %></a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown active">
                                        <a href="/Faculties.aspx"><%= Resources.Main.Faculties %></a>
                                    </li>
                                    <li class="dropdown active">
                                        <a href="/StudentLife.aspx"><%= Resources.Main.StudentLife %></a>
                                    </li>
                                    <li class="dropdown active">
                                        <a href="/ContactUs.aspx"><%= Resources.Main.ContactUs %></a>
                                    </li>
                                    <li class="dropdown active">
                                        <a href="/admin/default.aspx"><%= Resources.Main.AdminPanel %></a>
                                    </li>
                                    <li class="dropdown active">
                                        <asp:LinkButton ID="lnkLang" runat="server" OnClick="lnkLang_Click">
                                           <%= Resources.Main.Lang %>
                                        </asp:LinkButton>
                                    </li>
                                    <li class="dropdown active" runat="server" id="SignOut">
                                        <asp:LinkButton ID="lbkBtnSignOut" runat="server" OnClick="lbkBtnSignOut_Click">
                                            <%= Resources.Main.Logout %>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section id="maincontent">
            <div class="container">
                <asp:ContentPlaceHolder ID="MainContentPlaceHolder" runat="server">
                </asp:ContentPlaceHolder>
            </div>
        </section>

        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="span4">
                        <div class="widget">
                            <h5><%= Resources.Main.BrowsePages %></h5>
                            <ul class="regular">
                                <li><a href="/AboutUs.aspx"><%= Resources.Main.AboutUs %></a></li>
                                <li><a href="/Admissions.aspx"><%= Resources.Main.Admissions %></a></li>
                                <li><a href="/Colleges.aspx"><%= Resources.Main.Colleges %></a></li>
                                <li><a href="/StudentLife.aspx"><%= Resources.Main.StudentLife %></a></li>
                                <li><a href="/ContactUs.aspx"><%= Resources.Main.ContactUs %></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="widget">
                            <h5><%= Resources.Main.RecentBlogPosts %></h5>
                            <ul class="regular">
                                <li><a href="#"><%= Resources.Main.Post1%> </a></li>
                                <li><a href="#"><%= Resources.Main.Post2%> </a></li>
                                <li><a href="#"><%= Resources.Main.Post3%> </a></li>
                                <li><a href="#"><%= Resources.Main.Post4%> </a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="widget">
                            <address>
                                <strong><%= Resources.Main.MansouraUniversity%></strong><br>
                                <abbr title="Address"><%= Resources.Main.Address%></abbr>
                                <%= Resources.Main.Address2%><br>
                                <abbr title="Phone"><%= Resources.Main.Phone%></abbr>
                                <%= Resources.Main.Phone2%><br>
                                <abbr title="Fax"><%= Resources.Main.Fax%></abbr>
                                <%= Resources.Main.Fax2%>
                            </address>
                        </div>
                    </div>
                </div>
            </div>
            <div class="verybottom">
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <p>
                                &copy; <%= Resources.Main.Footer %> <%= DateTime.Now.Year %>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </form>
    <!-- JavaScript Library Files -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.easing.js"></script>
    <script src="assets/js/google-code-prettify/prettify.js"></script>
    <script src="assets/js/modernizr.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/jquery.elastislide.js"></script>
    <script src="assets/js/sequence/sequence.jquery-min.js"></script>
    <script src="assets/js/sequence/setting.js"></script>
    <script src="assets/js/jquery.prettyPhoto.js"></script>
    <script src="assets/js/application.js"></script>
    <script src="assets/js/jquery.flexslider.js"></script>
    <script src="assets/js/hover/jquery-hover-effect.js"></script>
    <script src="assets/js/hover/setting.js"></script>

    <!-- Template Custom JavaScript File -->
    <script src="assets/js/custom.js"></script>
</body>
</html>
