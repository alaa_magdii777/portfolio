﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSite.MasterPages
{
    public partial class AdminMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                if (HttpContext.Current.User.Identity.IsAuthenticated && IsAdmin())
                {
                    SignOut.Visible = true;

                }
                else
                {
                    Response.Redirect("/NotAllowed.aspx", false);
                }


            }
        }

        private bool IsAdmin()
        {
            return new BAL.Managers.UserManager().IsAdmin(Page.User.Identity.GetUserName());
        }

        protected void lbkBtnSignOut_Click(object sender, EventArgs e)
        {
            try
            {
                var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                authenticationManager.SignOut();
                Response.Redirect("/", false);
            }
            catch (Exception)
            {
            }

        }
    }
}