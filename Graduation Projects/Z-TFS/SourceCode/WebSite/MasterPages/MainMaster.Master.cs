﻿using System;
using System.Globalization;
using System.Threading;
using System.Web;

namespace WebSite.MasterPages
{
    public partial class MainMaster : System.Web.UI.MasterPage
    {

        public void InitCulture()
        {
            if(Session["language"] == null)
            {
                Session["language"] = "en-us";
                MyStyleSheet.Href = "/Assets/css/style.css";
            }
            else
            {
                String selectedLanguage = Session["language"].ToString();
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);

                if (Session["language"].ToString() == "en-us")
                {
                    MyStyleSheet.Href = "/Assets/css/style.css";
                }
                else
                {
                    MyStyleSheet.Href = "/Assets/css/styleAR.css";
                }
            }

            
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            

            if (!IsPostBack)
            {
               

                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    SignOut.Visible = true;
                    ulLogin.Visible = false;
                }
                else
                {
                    SignOut.Visible = false;
                    ulLogin.Visible = true;
                }

                InitCulture();
            }
        }

        protected void lbkBtnSignOut_Click(object sender, EventArgs e)
        {
            try
            {
                var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                authenticationManager.SignOut();
                Response.Redirect("/", false);
            }
            catch (Exception)
            {
            }

        }

        protected void lnkLang_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Session["language"].ToString()))
                {
                    if(Session["language"].ToString() == "en-us")
                    {
                        Page.UICulture = "ar-eg";
                        Page.Culture = "ar-eg";
                        Session["language"] = "ar-eg";
                        MyStyleSheet.Href = "/Assets/css/styleAR.css"; 
                    }
                    else
                    {
                        Page.UICulture = "en-us";
                        Page.Culture = "en-us";
                        Session["language"] = "en-us";
                        MyStyleSheet.Href = "/Assets/css/style.css";
                    }
                }
                else
                {
                    Page.UICulture = "en-us";
                    Page.Culture = "en-us";
                    Session["language"] = "en-us";
                }

                Response.Redirect(Request.RawUrl, false);
            }
            catch (Exception ex)
            {
            }
        }
    }
}