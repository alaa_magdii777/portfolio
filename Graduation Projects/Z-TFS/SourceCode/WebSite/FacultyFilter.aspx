﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="FacultyFilter.aspx.cs" Inherits="WebSite.FacultyFilter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <title><%= Resources.Main.ChooseFaculty %></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <section id="subintro">
        <div class="jumbotron subhead" id="overview">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="centered">
                            <h3><%= Resources.Main.ChooseFaculty %></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb notop">
                        <li><a href="/"><%= Resources.Main.Home %></a><span class="divider">/</span></li>
                        <li class="active"><%= Resources.Main.ChooseFaculty %></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="maincontent">
        <div class="container">
            <div class="row">
                <div runat="server" id="divMessage"></div>
            </div>
            <div class="row">
                <div class="span12">
                    <div class="row">
                        <div class=" span12 form-group">
                            <asp:Label runat="server" AssociatedControlID="rblFaculty"><%= Resources.Main.ChooseFaculty %><span style="color:red">*</span></asp:Label>
                            <asp:RadioButtonList runat="server" ID="rblFaculty" CssClass=" span12 radio ">

                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="rfvrblFaculty" runat="server" ValidationGroup="Choose"
                                ErrorMessage="<%$ Resources:Main, Required %>" Text="<%$ Resources:Main, Required %>" ControlToValidate="rblFaculty" CssClass="validation"></asp:RequiredFieldValidator>
                        </div>
                        <div class=" span12 form-group text-center">
                            <asp:Button runat="server" ID="btnApply" CssClass="btn btn-block btn-success"
                                ValidationGroup="Choose" Text="<%$ Resources:Main, Apply %>" OnClick="btnApply_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
