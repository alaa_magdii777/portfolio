﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="OurValues.aspx.cs" Inherits="WebSite.Pages.OurValues" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <title>Our Values</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <section id="subintro">
        <div class="jumbotron subhead" id="overview">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="centered">
                            <h3>Our Values</h3>                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb notop">
                        <li><a href="/">Home</a><span class="divider">/</span></li>
                        <li class="active">Our Values</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
