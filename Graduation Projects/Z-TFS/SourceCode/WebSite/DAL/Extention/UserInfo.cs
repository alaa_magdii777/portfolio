﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.DAL
{
    public partial class UserInfo
    {
        public string Faculty
        {
            get
            {
                var user = new BAL.Managers.UserManager().GetUserInformation();
                var context = new DAL.AcademicDataContext();

                var faculty = context.UserFaculties.OrderByDescending(u => u.Id).FirstOrDefault(u => u.UserId == UserId);

                return faculty != null ? faculty.Faculty.FacultyName : string.Empty;

            }
        }
    }
}