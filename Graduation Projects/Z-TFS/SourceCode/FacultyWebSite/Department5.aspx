﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="Department5.aspx.cs" Inherits="FacultyWebSite.Department5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" runat="server">
    <title>
       <%= Resources.Main.Department5 %>
    </title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <section id="subintro">
        <div class="jumbotron subhead" id="overview">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="centered">
                            <h3><%= Resources.Main.Department5 %></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <ul class="breadcrumb notop">
                        <li><a href="/"><%= Resources.Main.Home %></a><span class="divider">/</span></li>
                        <li class="active"><%= Resources.Main.Department5 %></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
     <div class="flexslider">
                        <ul class="slides">
                            <li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; display: list-item;">
                                <img style="max-height: 450px !important" src="/assets/img/slides/flexslider/55.jpeg" alt="">
                            </li>
                            <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;" class="">
                                <img style="max-height: 450px !important" src="/assets/img/slides/flexslider/555.jpeg" alt="">
                          
                        </ul>

                        <ul class="flex-direction-nav">
                            <li><a class="flex-prev" href="#">Previous</a></li>
                            <li><a class="flex-next" href="#">Next</a></li>
                        </ul>
                    </div>
    <section>
      <%= Resources.Main.DummyContents %>
    </section>
    <section>
        <p>
            <a href="/Assets/Files/Doc1.pdf">Download File 1</a>
        </p>
        <p>
            <a href="/Assets/Files/Doc2.pdf">Download File 2</a>
        </p>
        <p>
            <a href="/Assets/Files/Doc3.pdf">Download File 3</a>
        </p>
    </section>
</asp:Content>



