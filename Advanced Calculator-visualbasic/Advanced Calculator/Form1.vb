﻿Public Class Form1
    Dim ClearDisplay As Boolean
    Dim Operand1 As Double
    Dim Operand2 As Double
    Dim Operatar As String
 
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click, Button2.Click, Button3.Click, Button4.Click, Button5.Click, Button6.Click, Button7.Click, Button8.Click, Button9.Click, Button11.Click
        If ClearDisplay Then
            TextBox1.Text = ""
            ClearDisplay = False
        End If
        TextBox1.Text += sender.Text
    End Sub

    Private Sub Button14_Click(sender As Object, e As EventArgs) Handles Button14.Click
        TextBox1.Text = ""
    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        If TextBox1.Text.IndexOf(".") > 0 Then
            Exit Sub
        Else
            TextBox1.Text = TextBox1.Text & "."
        End If
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        Operand1 = Val(TextBox1.Text)
        Operatar = "+"
        ClearDisplay = True
    End Sub

    Private Sub Button15_Click(sender As Object, e As EventArgs) Handles Button15.Click
        Operand1 = Val(TextBox1.Text)
        Operatar = "-"
        ClearDisplay = True
    End Sub

    Private Sub Button16_Click(sender As Object, e As EventArgs) Handles Button16.Click
        Operand1 = Val(TextBox1.Text)
        Operatar = "*"
        ClearDisplay = True
    End Sub

    Private Sub Button17_Click(sender As Object, e As EventArgs) Handles Button17.Click
        Operand1 = Val(TextBox1.Text)
        Operatar = "/"
        ClearDisplay = True
    End Sub

    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        Dim result As Double
        Operand2 = Val(TextBox1.Text)
        Try
            Select Case Operatar
                Case "+"
                    result = Operand1 + Operand2
                Case "-"
                    result = Operand1 - Operand2
                Case "*"
                    result = Operand1 * Operand2
                Case "/"
                    If Operand2 <> "0" Then
                        result = Operand1 / Operand2
                    End If
                Case "\"
                    If Operand2 <> "0" Then
                        result = Operand1 \ Operand2
                    End If
            End Select
            TextBox1.Text = result
        Catch exc As Exception
            MsgBox(exc.Message)
            TextBox1.Text = "ERROR"
        Finally
            ClearDisplay = True
        End Try
    End Sub

    Private Sub Button21_Click(sender As Object, e As EventArgs) Handles Button21.Click
        TextBox1.Text = Math.Sin(TextBox1.Text)
        ClearDisplay = True
    End Sub

    Private Sub Button20_Click(sender As Object, e As EventArgs) Handles Button20.Click
        TextBox1.Text = Math.Cos(TextBox1.Text)
    End Sub

    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click
        TextBox1.Text = Math.Tan(TextBox1.Text)
    End Sub

    Private Sub Button24_Click(sender As Object, e As EventArgs) Handles Button24.Click
        If Val(TextBox1.Text) < 0 Then
            MsgBox("Can't calculate the logarithm of a negative number")
        Else
            TextBox1.Text = Math.Log(TextBox1.Text)
        End If
        ClearDisplay = True
    End Sub

    Private Sub Button18_Click(sender As Object, e As EventArgs) Handles Button18.Click
        TextBox1.Text = Math.Sqrt(Val(TextBox1.Text))
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class
