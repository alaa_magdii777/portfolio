import React, { Component,useMemo,useState } from 'react';
import configureStore from "./src/store/configureStore";
import { Provider } from 'react-redux';
import { SafeAreaProvider } from "react-native-safe-area-context";
import MainNavigation from "./src/navigations/MainNavigation";
import 'react-native-gesture-handler';
// import AuthContext from "./src/contexts/AuthContext"; 

const store = configureStore()
const App = () => { 
  // const [authData, setAuthData] = useState({});
  // const [refreshAuthData, setRefreshAuthData] = useState({});
  // const authContextValue = useMemo(() => ({ authData, setAuthData }), [authData]);

  // render() {
  return (
    // <AuthContext.Provider value={authContextValue}>
      // <SafeAreaProvider >

      <Provider store={store}>
        <MainNavigation />
      </Provider>

      // </SafeAreaProvider>
    // {/* </AuthContext.Provider> */}

  )
}

export default App;




