export default {

    get logout(){
        return require('./assets/images/logout.png'); 
    },
    get drawer(){
        return require('./assets/images/drawer.png'); 
    },
    get graduate(){
        return require('./assets/images/graduate.png'); 
    },
    get email(){
        return require('./assets/images/mail.png'); 
    },
    get password(){
        return require('./assets/images/lock.png'); 
    },
    get check_active(){
        return require('./assets/images/check_active.png'); 
    },
    get check_inactive(){
        return require('./assets/images/check_inactive.png'); 
    },
    get bg(){
        return require('./assets/images/bg.png'); 
    },
    get avatar_yellow(){
        return require('./assets/images/avatar_yellow.png'); 

    }

}