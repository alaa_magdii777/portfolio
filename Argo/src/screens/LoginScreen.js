import React, { useState, useContext } from 'react';
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity, ScrollView, ImageBackground, ActivityIndicator } from 'react-native';
import { Button } from 'react-native-elements';
import AuthContext from "../contexts/AuthContext";
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import RefreshAuth from "../contexts/RefreshAuth";
import Images from "../Images";
import Font from "../utils/fonts";
import colorsTheme from "../utils/colorsTheme";
// import {login} from '../store/actions/auth'



const { Family } = Font;
const { Size } = Font;
const { colors } = colorsTheme

const data = {
    "username": "test40",
    "password": "12345678",
}

const LoginScreen = ({ navigation }) => {
    const { authData, setAuthData } = useContext(AuthContext);
    // const {refreshAuthData, setRefreshAuthData} = useContext(RefreshAuth);

    //alert(JSON.stringify(auth))

    const Login = () => {
        axios.post(`http://youcast.media/api/login_check`, data, {
            headers:
                [
                    {
                        "key": "Content-Type",
                        "name": "Content-Type",
                        "value": "application/json",
                        "type": "text",
                    }
                ],


        })
            .then(response => {
                setAuthData(response.data)
                alert(JSON.stringify(response.data))
                console.log(response.data);
                navigation.navigate('home'),
                    AsyncStorage.setItem('@currentUser', JSON.stringify(response.data));


            })
            .catch(error => {
                alert(error.message)
            })
    }
 
    return (
        <ScrollView>
            <View style={{ flex: 1, alignItems: "center", marginTop: 40 }}>

                <ImageBackground
                    style={{ height: 63, width: 220, alignItems: "center" }}
                    source={require('../assets/images/logo.png')}
                />



                <View style={styles.up_form}>
                    <View style={styles.form}>
                        <View style={styles.form_up_inputs}>
                            

                                        <View style={styles.up_input}>
                                            <Image style={styles.input_icon} source={Images.graduate} />
                                            <TextInput
                                                style={styles.input}
                                                placeholder='userName'
                                                 />
                                        </View>


                                        <View style={styles.up_input}>
                                            <Image style={styles.input_icon} source={Images.password} />
                                            <TextInput
                                                style={styles.input}
                                                placeholder='Password'
                                             />
                                        </View>


                                      
                                                <React.Fragment>
                                                    <TouchableOpacity 
                                                        activeOpacity={.9}
                                                        style={styles.button} onPress={() =>Login()}>
                                                        <Text style={styles.buttonText}>Login</Text>
                                                    </TouchableOpacity>
                                                    
                                                </React.Fragment>
                                           

                        </View>
                    </View>
                </View>

            </View>
        </ScrollView>
    )
}


LoginScreen.defaultProps = {};


const styles = StyleSheet.create({


    up_form: {
        width: '100%',
        elevation: 20,
        maxWidth: 400,
        marginBottom: 100,
    },
    form: {
        flex: 1,
        // backgroundColor : '#fff',
        borderRadius: 15,
        borderWidth: 5,
        borderColor: '#d7d7d7',
        height: 320,
        padding: 20,
        paddingHorizontal: 0,
        margin: 20,
        marginTop: 40,
    },

    form_up_inputs: {
        paddingHorizontal: 20,
    },
    title: {
        textAlign: 'center',
        color: '#101010',
        fontSize: 16,
        lineHeight: 22,
        fontFamily: Font.Family.regular,
    },
    up_input: {
        height: 50,
        maxHeight: 50,
        overflow: 'hidden',
        borderRadius: 5,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#DCDCDC',
        marginTop: 20,
        backgroundColor: '#fff',
    },
    input: {
        flex: 1,
        lineHeight: 35,
        paddingVertical: 0,
        fontFamily: Font.Family.regular,
    },
    input_icon: {
        height: 22,
        width: 22,
        alignSelf: 'center',
        marginHorizontal: 5,
        resizeMode: 'contain'
    },

    button: {
        flex: 1,
        height: 35,
        paddingHorizontal: 20,
        borderRadius: 6,
        borderColor: '#A50560',
        borderWidth: 1,
        backgroundColor: colors.purpleLight,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30,
        paddingVertical: 25,
    },

    buttonText: {
        color: '#fff',
        fontFamily: Font.Family.semi_bold,
        fontSize: 18
    },

})


export default LoginScreen;