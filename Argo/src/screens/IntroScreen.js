// import React, { Component } from 'react'
// import { Text, View,ScrollView} from 'react-native'
// import { Button } from 'react-native-elements';
// import Icon from 'react-native-vector-icons/FontAwesome';

// export default class IntroScreen extends Component {
//     constructor(props){
//         super(props);   
//     } 
//     render() {
//         let {navigation}=this.props;
//         return (
//             <ScrollView>
//             <View style={{flex:1,alignItems:"center",marginTop:40}}>
//                 <Text style={{fontSize:20,paddingVertical:10}}>intro </Text>
//                 <Button onPress={() => navigation.navigate('login')}
//                     title="go to login"
//                     containerStyle={{alignContent:"center"}}
//                     />
//             </View>
//             </ScrollView>
//         )
//     }
// }



import React from "react";
import {
    ImageBackground,
    Image,
    StyleSheet,
    StatusBar,
    Dimensions
  } from "react-native";
import { Block, Button, Text, theme } from "galio-framework";

const { height, width } = Dimensions.get("screen");

// import argonTheme from "../constants/Theme";
import Images from "../Images";

class IntroScreen extends React.Component {
  getApp = async() => {
    return new Promise((resolve) =>
      setTimeout(
        () => { resolve('result') },
        3000
      )
    )
  }
  
  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage
    const data = await this.getApp();

    if (data !== null) {
      this.props.navigation.navigate('login');
    }
  }
  render() {
    const { navigation } = this.props;
    return (
      <Block flex style={styles.container}>
        <StatusBar hidden />
        <Block flex center>
        <ImageBackground
            source={Images.bg}
            style={{ height, width, zIndex: 1 }}
          />
        </Block>
        <Block center>
          <Image source={require('../assets/images/logo.png')} style={styles.logo} />
        </Block>
        <Block flex space="between" style={styles.padded}>
            <Block flex space="around" style={{ zIndex: 2 }}>
              <Block style={styles.title}>
                {/* <Block>
                  <Text color="white" size={60}>
                    ITShare Academy
                  </Text>
                </Block> */}
                {/* <Block>
                  <Text color="white" size={60}>
                    System
                  </Text>
                </Block> */}
                {/* <Block style={styles.subTitle}>
                  <Text color="white" size={16}>
                    Largest Provider of Official IT Training.
                  </Text>
                </Block> */}
              </Block>
              {/* <Block center>
                <Button
                  style={styles.button}
                  color={argonTheme.COLORS.SECONDARY}
                  onPress={() => navigation.navigate("App")}
                  textStyle={{ color: argonTheme.COLORS.BLACK }}
                >
                  Get Started
                </Button>
              </Block> */}
          </Block>
        </Block>
      </Block>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.COLORS.BLACK
  },
  padded: {
    paddingHorizontal: theme.SIZES.BASE * 2,
    position: "relative",
    bottom: theme.SIZES.BASE,
    zIndex: 2,
  },
  button: {
    width: width - theme.SIZES.BASE * 4,
    height: theme.SIZES.BASE * 3,
    shadowRadius: 0,
    shadowOpacity: 0
  },
  logo: {
    width: 220,
    height: 60,
    zIndex: 2,
    position: 'relative',
    marginTop: '-50%'
  },
  title: {
    marginTop:'-5%'
  },
  subTitle: {
    marginTop: 20
  }
});

export default IntroScreen;

