import React, { Component } from 'react'
import { Text, View, FlatList, ScrollView, TextInput, StyleSheet, Image } from 'react-native'
import FeedBackCard from "../components/FeedBackCard";
import Images from "../Images";
import AuthContext from "../contexts/AuthContext";
import axios from 'axios' 

const keyExtractor = ({ id }) => id

export default class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }


    contacts = [
        { id: '1', name: 'Alaa Magdi Test App', avatar: (Images.avatar_yellow), title: "Test Authorization", feedback: "The Test Authorization is good", },
        { id: '2', name: 'Alaa Magdi Test App', avatar: (Images.avatar_yellow), title: "Test Authorization", feedback: "The Test Authorization is good" },
        { id: '3', name: 'Alaa Magdi Test App', avatar: (Images.avatar_yellow), title: "Test Authorization", feedback: "The Authorization is fine" },
        { id: '4', name: 'Alaa Magdi Test App', avatar: (Images.avatar_yellow), title: "Test Authorization", feedback: "The Authorization is fine" },
        { id: '5', name: 'Alaa Magdi Test App', avatar: (Images.avatar_yellow), title: "Test Authorization", feedback: "The Authorization is fine" },
    ]



    renderContact = ({ item }) => {
        const { id, name, avatar, title, feedback } = item;
        let { navigation } = this.props;

        return (
            <FeedBackCard
                name={name}
                avatar={avatar}
                title={title}
                feedback={feedback}
            />
        );
    };


  //  static contextType=AuthContext;

//    componentDidMount(){
//        const token = this.context.authData.token

//        axios.get(`http://youcast.media/api/models/list-posts`,{
//         headers:[
//         // "auth": {
//             {"type": "bearer",
//             "bearer": [
//                 {
//                     "key": `${token}`,
//                     // "value": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1ODIyMTYzODQsImV4cCI6MTU4MjU3NjM4NCwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoidGVzdDQwIn0.oWyDBreYSzSuorVPbAtGcRI5ksrbYLSkxBFKQfVmqRtjMC7C82kiP_qWF5vkzv2zrvfLaVZvxbu4rtol80gUr3EUJmZh-DfeyjRF0XHHvOxe0uq11d8LcLvQy91778_pBx5OxakZLwi7pBeRmCfL_uyOKX_8bR2Wq9N6zg6ancQxRon-Qi-dcaMlAQGSNRym637TSW27lQWHZ7U6GUouLnN-w9rWQ1yQsa4Kvl_3DGSj2MRboHhCHMgc7pOfSasw-8srd9fiG__Kk4ez5fhqDFUeC3pubd7b7skztHz1gUIHNL_WD4BOtaZZjLMYPBttTrxzjtHNnUkDjzvf1c_s7A",
//                     // "type": "string",
//                 }
//             ],
            
            
//         // },
//         }
//         ]
//     })
//     .then(response=>{
       
//         alert(JSON.stringify(response.data),
//         console.log(refresh_token,"aaaaaaaaaaaaaa")
        
//     )})
//     .catch(error => {
//         alert(error.message)
//     })
//    }

    render() {
        let { navigation } = this.props;
        return (
            <React.Fragment>

                <ScrollView style={{ flex: 1, marginHorizontal: 15, marginVertical: 15 }}>
                    <View style={{ marginVertical: 20, flexDirection: "row" }}>

                    </View>

                    <View>
                        <FlatList style={{ backgroundColor: 'white', marginBottom: 100, borderWidth: .2, borderRadius: 5 }}
                            data={this.contacts}
                            renderItem={this.renderContact}
                            keyExtractor={keyExtractor} />
                    </View>


                </ScrollView>
            </React.Fragment>

        )
    }
}


const styles = StyleSheet.create({
    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 10, 
        flexDirection: "column",
        backgroundColor: '#fff', 
        borderWidth: .2,
        color: '#424242', 
        borderRadius: 5,

    },

})


// export default class HomeScreen extends Component {
//     constructor(props) {
//         super(props);
//     }
//     render() {
//         let { navigation } = this.props;
//         return (
//             <ScrollView>
//              {/* <Header navigation={navigation} /> */}
//             <View style={{flex:1,alignItems:"center",marginTop:40}}>
//                 <Text style={{color:colors.primary,fontSize:Font.Size.fs_19,paddingVertical:10,fontFamily:Font.Family.semi_bold,paddingVertical:20}}>Welcome in Home</Text>
//             </View>
//             </ScrollView>
//         )
//     }
// }




