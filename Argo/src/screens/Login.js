import React, {Component, useState, useContext } from 'react';
import { View,Image,StyleSheet ,TouchableWithoutFeedback,ImageBackground,Keyboard,I18nManager,ScrollView} from 'react-native';
import {Button,Icon,Input,CheckBox} from 'react-native-elements';
import SecuredFormikInput from "../utils/ui/SecuredFormikInput";
import { Formik } from 'formik';
import { connect } from 'react-redux';
import * as yup from 'yup';
import AsyncStorage from '@react-native-community/async-storage';
import RNRestart from 'react-native-restart';
import colorsTheme from "../utils/colorsTheme";
import Font from "../utils/fonts";
import PhoneInput from "../components/PhoneInput"; 
import FormikInput from "../components/FormikInput"; 
import axios from 'axios' 
import I18n from "../locals/i18n";

// import {login} from '../store/actions/auth'

const validationSchema = yup.object().shape({
    mobile_num: yup
      .string()
      .required(()=>`${I18n.t('username-req')}`),
    password: yup
      .string()
      .required(()=>`${I18n.t('password-req')}`)
      .min(3,()=>`${I18n.t('password-min')}`),
    country_code:yup.
      string().
      required(),
 
      //add countrycode
  });



 class Login extends Component {

     constructor(props){
         super(props);
         this.state={
            

         }
     }



    // handleChangeLang=async()=>{
    //     let current_lang=this.props.Lang;
    //     if(current_lang.includes("ar"))
    //     {
    //         try
    //         {
    //             await AsyncStorage.setItem('currentLang',"en")      
    //             if(I18nManager.isRTL)
    //             {
    //                 I18nManager.forceRTL(false);
    //                 I18nManager.allowRTL(false);
    //             }             
    //             RNRestart.Restart();
                         
    //         }
    //         catch (e) {
    //             console.log("some error happened",e)
    //         }
    //     }
    //     else if(current_lang.includes("en"))
    //     {
    //         try
    //         {
    //             await AsyncStorage.setItem('currentLang',"ar")  
    //             if(!I18nManager.isRTL)
    //             {
    //                 I18nManager.forceRTL(true);
    //                 I18nManager.allowRTL(true);    
    //             }               
    //             RNRestart.Restart();      
    //         }
    //         catch (e) {
    //             console.log("some error happened",e)
    //         }
    //     }
    // }

   render(){   
   
    let {navigation}=this.props
    // let btn_title=this.props.Lang.includes("ar")?"English":"العربية"
    //  const loginSuccuess=()=> navigation.reset({index:0,routes:[{name:'home'}]})  
    // const loginSuccuess=()=> {navigation.navigate('Home') } 

    return(
        <ScrollView style={{flex:1,backgroundColor:colorsTheme.colors.primary}}>
        <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()} style={{flex:1}}>

        <View style={{flex:1,alignItems:"center",}}>
            <View style={{flexDirection:"row",alignSelf:"center",marginTop:70}}>
              
                <ImageBackground
                    style={{ height: 58, width: 200,}}
                    source={require('../assets/images/logo.png')}
                />
            </View>
            
            {/* <Button
                title={btn_title}    
                buttonStyle={styles.btn_title} 
                titleStyle={styles.btn_title}                  
                containerStyle={{position:"absolute",top:20,right:30 }}
                onPress={this.handleChangeLang}
            /> */}
    
            <View style={styles.cardLogin}>
            <Formik

                //initialValues={{ mobile_num: '', password: '' }}
                initialValues={{ mobile_num: '',password: '',country_code:'+966' }}
                onSubmit={async(values, actions) => {
                    console.log("values",values)
                    // await this.props.onLogin({mobileNum: values.mobile_num,password:values.password,loginSuccuess})
                    setTimeout(() => {
                        actions.setSubmitting(false);
                        }, 1000);
                        navigation.navigate('home')
    
                }}
                validationSchema={validationSchema}
                >
                    {formikProps=>(
                    <React.Fragment>
                       
                        <FormikInput
                            formikProps={formikProps}
                            formikKey="mobile_num"
                        />
 
                       
                        <SecuredFormikInput
                            placeholder={I18n.t('password')}  
                            formikProps={formikProps}
                            formikKey="password"
                        />
                        {formikProps.isSubmitting ? (
                                <Button 
                                    loading 
                                    containerStyle={{margin:30}}
                                    buttonStyle={{backgroundColor:colorsTheme.colors.purpleLight}}
                                    titleStyle={{color:'white'}}
                                    containerStyle={{margin:30}}
                                   onPress={()=>{navigation.navigate('home')}}
                                />
                            ) : (

                                <Button title={I18n.t('login')}   
                                    onPress={formikProps.handleSubmit} 
                                    buttonStyle={{backgroundColor:colorsTheme.colors.purpleLight}}
                                    titleStyle={{color:'white',fontFamily:Font.Family.semi_bold}}
                                    containerStyle={{margin:30}}
                                />
                            )}
                    </React.Fragment>
                    )}
                </Formik>
            </View>
    
            {/* <Button
                title='forget-password'
                type="clear"
                buttonStyle={{backgroundColor:"white"}}
                containerStyle={{alignSelf:'flex-start',marginStart:30,marginBottom:30}}
                titleStyle={{fontSize:Font.Size.fs_14,fontFamily:Font.Family.regular,color:"black"}}
                 onPress={()=>{navigation.navigate('EnterPhone')}}
            /> */}
            <View style={{marginStart:30,marginBottom:30}}/>
           
                
        </View>
        </TouchableWithoutFeedback>
        </ScrollView>
  )
}
}

   

Login.defaultProps = {};


const styles=StyleSheet.create({
    cardLogin:{
        backgroundColor:"white",
        borderRadius:10,
        height:250,
        width:"84%",
        // marginTop: -20,
        marginTop:30,
        paddingHorizontal:15,
        elevation:5
    },
    errorMsg:{
        color:colorsTheme.colors.textRed,
        fontSize:Font.Size.fs_10,
        fontFamily:Font.Family.regular,
        marginTop:5,
    },
    btn_title:{
       color:colorsTheme.colors.purpleLight,
       backgroundColor:"transparent"
    }
})


const mapStateToProps = state => ({
    
    Lang: state.preferences.language,
  });
   
const mapDispatchToProps = dispatch => ({

});
  
export default connect(mapStateToProps, mapDispatchToProps)(Login)
