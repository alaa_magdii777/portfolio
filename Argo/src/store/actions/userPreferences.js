import {CHANGE_LANGUAGE} from './index';

export const userPreferences = Lang => (
    {
      type: CHANGE_LANGUAGE,
      payload: Lang,
    }
  );
