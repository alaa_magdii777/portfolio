import { createStore, combineReducers } from 'redux';
import userPreferences from './reducers/userPreferences';
const rootReducer = combineReducers(
    { preferences: userPreferences }
);
const configureStore = () => {
return createStore(rootReducer);
}
export default configureStore;
