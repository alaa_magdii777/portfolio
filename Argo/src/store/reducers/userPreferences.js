import {CHANGE_LANGUAGE} from '../actions/index';
import {getCurrentLocale} from '../../locals/i18n';

const initialState = {language:getCurrentLocale()};

const userPreferences = (state = initialState, action) => {
  switch (action.type) {
  case CHANGE_LANGUAGE: {
    return {...state, language: action.payload};
  }
  default:
    return state;
  }
};

export default userPreferences;
