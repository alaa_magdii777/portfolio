export default {  
    "username-req":"أدخل أسم المستخدم",
    "password-req":"أدخل الرقم السرى",
    "password-min":"البسورد أقل",
    "enter user name":"أدخل أسم المستخدم",
    "password":"البسورد",
    "login":"أدخل"
}