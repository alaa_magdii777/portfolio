import I18n from 'i18n-js';
import en from './en';
import ar from './ar';

I18n.fallbacks = true;
//  I18n.locale="ar";
I18n.translations = {
  en,
  ar
};



export const setLocale = (locale) => {
  I18n.locale = locale;
};
export const getCurrentLocale = () => I18n.locale;


export default I18n;