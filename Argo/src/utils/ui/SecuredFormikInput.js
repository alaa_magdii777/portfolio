import React,{Component} from 'react';
import {Text,Icon,Input} from "react-native-elements"
//import I18n from "../locals/i18n"
import colorsTheme from "../colorsTheme";
import Font from "../fonts"
import { connect } from 'react-redux';

class SecuredFormikInput extends Component{
    state={
        entered_txt:null,
        placeholder:true,
        secured:true
    }

    toggleSecured=()=>{
        this.setState({secured:!this.state.secured})
    }

    render(){
        let {formikProps,formikKey,placeholder}=this.props
        return(
            <Input
            value={this.state.entered_txt}
            secureTextEntry={this.state.secured}
            onChangeText={value => {
              formikProps.setFieldValue(formikKey, value);
              this.setState({entered_txt:value,placeholder:value.length==0})
            }}
            onBlur={formikProps.handleBlur(formikKey)}
            placeholder={placeholder}
            inputStyle={{
                textAlign:this.props.Lang.includes('ar')?"right":"left",
                fontSize:Font.Size.fs_13,
                fontFamily:Font.Family.regular
            }}
            inputContainerStyle={{
                borderBottomWidth:1,
                borderBottomColor:colorsTheme.colors.placeholderColor,             
            }}

            placeholderTextColor={colorsTheme.colors.placeholderColor}
            errorMessage={formikProps.touched[formikKey] && formikProps.errors[formikKey]}
            rightIcon={
                <Icon
                    type="entypo"
                    color="grey"
                    size={15}
                    name="eye"
                    onPress={this.toggleSecured}
                />
            }
          />
           
        )

    }
 
}

const mapStateToProps = state => ({   
    Lang: state.preferences.language,
  });
   
const mapDispatchToProps = dispatch => ({

});
  
export default connect(mapStateToProps, mapDispatchToProps)(SecuredFormikInput)
