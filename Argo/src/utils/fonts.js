import {I18nManager} from "react-native"
 
 const fontTheme_ar = {
    Size:{
        fs_10:10,
        fs_11:11,
        fs_12:12,
        fs_13:13,
        fs_14:14,
        fs_15:15,
        fs_16:16,
        fs_17:17,
        fs_18:18,
        fs_19:19,
        fs_20:20,
    },
    Family: {
        regular:"Almarai-Light",
        medium:"Almarai-Regular",
        semi_bold:"Almarai-Bold"
    },
  };
  
  const fontTheme_en = {
    Size:{
        fs_10:10,
        fs_11:11,
        fs_12:12,
        fs_13:13,
        fs_14:14,
        fs_15:15,
        fs_16:16,
        fs_17:17,
        fs_18:18,
        fs_19:19,
        fs_20:20,
    },
    Family: {
        regular:"Poppins-Regular",
        medium:"Poppins-Medium",
        semi_bold:"Poppins-SemiBold"
    },
  };
     
  const Font=I18nManager.isRTL?fontTheme_ar:fontTheme_en
  
  export default Font;


