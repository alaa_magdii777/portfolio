import * as React from 'react';
import { View, Text,Image} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import IntroScreen from "../screens/IntroScreen";
import LoginScreen from "../screens/LoginScreen";
import Login from "../screens/Login";
import HomeScreen from "../screens/HomeScreen";
// import Header from "../components/Header";
// import Drawer from "../navigations/drawer/Drawer";
import Images from "../Images";

const Stack = createStackNavigator();


function MainNavigation() {
  return (
    <NavigationContainer>
        <Stack.Navigator headerMode="none" initialRouteName="intro">
        <Stack.Screen name="intro" component={IntroScreen} />
        <Stack.Screen name="login" component={Login} />
        <Stack.Screen name="home" component={HomeScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default MainNavigation;
