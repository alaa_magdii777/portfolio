import React from 'react'
import Snackbar from 'react-native-snackbar';
import colorsThemes from "../utils/colorsTheme"
import Font from "../utils/fonts"

//---
const {colors}=colorsThemes
const {Family}=Font

const showSnackBar=({msg,background_color})=>{
    Snackbar.show({
        text:msg,
        backgroundColor:background_color,
        textColor:colors.white,
        fontFamily:Family.regular
    })
}

export default showSnackBar;
