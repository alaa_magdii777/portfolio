import React, { useState } from 'react'
import { View, StyleSheet,I18nManager,Modal  } from 'react-native'
import CountryPicker from 'react-native-country-picker-modal'
import {Input,Text} from 'react-native-elements';
import colorsTheme from "../utils/colorsTheme"
import Font from "../utils/fonts"
 

const {colors}=colorsTheme
const {Family,Size}=Font
const styles = StyleSheet.create({
    input:{
      fontSize:Size.fs_13,
      fontFamily:Family.medium,
    },
    containerInput:{
      borderBottomWidth:1,
      borderBottomColor:colors.placeholderColor,   
      padding:0,          
  },
  callingCodeEn:{
    marginLeft:-5,
    marginTop:5,
    marginRight:5,
    fontSize:Size.fs_13,
    fontFamily:Family.medium,
    color:"black"
  },
  callingCodeAr:{
    marginLeft:-5,
    marginTop:5,
    marginRight:5,
    fontSize:Size.fs_13,
    fontFamily:Family.medium,
    color:"black"
  }
})



export default function PhoneInput({formikProps, formikKey}) {
  const [countryCode, setCountryCode] = useState('SA')
  const [country, setCountry] = useState(null)
  const [withCountryNameButton, setWithCountryNameButton] = useState( false)
  const [withFlag, setWithFlag] = useState(true)
  const [withEmoji, setWithEmoji] = useState(true)
  const [withFilter, setWithFilter] = useState(true)
  const [withAlphaFilter, setWithAlphaFilter] = useState(false)
  const [withCallingCode, setWithCallingCode] = useState(true)
  const onSelect = (country) => {
    setCountryCode(country.cca2)
    setCountry(country)
    formikProps.setFieldValue('country_code','+'+country.callingCode[0])
  }
  return (
        <Input
            placeholder='mobile-num'  
            inputStyle={styles.input}
            inputContainerStyle={styles.containerInput}
            keyboardType={'numeric'}
            onChangeText={value => {
              formikProps.setFieldValue(formikKey,value);
            }}
            onBlur={formikProps.handleBlur(formikKey)}
            errorMessage={formikProps.touched[formikKey] && formikProps.errors[formikKey]}
            rightIcon={
                I18nManager.isRTL?
                <React.Fragment>
                   {/* {country !== null && (
                    <Text style={styles.callingCodeAr}>{country.callingCode}</Text>
                   )} */}
                  <CountryPicker

                      {...{
                      countryCode,
                      withFilter,
                      withFlag,
                      withCountryNameButton,
                      withAlphaFilter,
                      withCallingCode,
                      withEmoji,
                      onSelect,
                      }}
                      filterProps={{
                        placeholder:'select-country',
                      }}
                      modalProps={{transparent:true}}
                      translation='urd'
                  />
                 
                </React.Fragment>
                :null
                
            }
            rightIconContainerStyle={{marginRight:-10,flexDirection:"row"}}
            leftIcon={
              I18nManager.isRTL?null:
              <React.Fragment>
              <CountryPicker
              {...{
              countryCode,
              withFilter,
              withFlag,
              withCountryNameButton,
              withAlphaFilter,
              withCallingCode,
              withEmoji,
              onSelect,
              }}
              filterProps={{
                placeholder:'select-country',
              }}
              modalProps={{transparent:true}}
              translation='common'
            />
             
            </React.Fragment>
            }
            leftIconContainerStyle={{marginLeft:-5,flexDirection:"row",}}
        />


  )
}
