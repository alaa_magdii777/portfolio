import React, { useState } from 'react'
import { View, StyleSheet,I18nManager,Modal, TextInput  } from 'react-native'
import CountryPicker from 'react-native-country-picker-modal'
import {Input,Text} from 'react-native-elements';
import colorsTheme from "../utils/colorsTheme"
import Font from "../utils/fonts"
import I18n from "../locals/i18n";

const {colors}=colorsTheme
const {Family,Size}=Font
const styles = StyleSheet.create({
    input:{
      fontSize:Size.fs_13,
      fontFamily:Family.medium,
      paddingTop:10
    },
    containerInput:{
      borderBottomWidth:1,
      borderBottomColor:colors.placeholderColor,   
      padding:0,          
  },
  callingCodeEn:{
    marginLeft:-5,
    marginTop:5,
    marginRight:5,
    fontSize:Size.fs_13,
    fontFamily:Family.medium,
    color:"black"
  },
  callingCodeAr:{
    marginLeft:-5,
    marginTop:5,
    marginRight:5,
    fontSize:Size.fs_13,
    fontFamily:Family.medium,
    color:"black"
  }
})



export default function FormikInput({formikProps, formikKey}) {
 
  return (
        <Input
            placeholder={I18n.t('enter user name')}  
            inputStyle={styles.input}
            inputContainerStyle={styles.containerInput}
            // keyboardType={'numeric'}
            onChangeText={value => {
              formikProps.setFieldValue(formikKey,value);
            }}
            onBlur={formikProps.handleBlur(formikKey)}
            errorMessage={formikProps.touched[formikKey] && formikProps.errors[formikKey]}
            
        />


  )
}
